use enum_map::Enum;
use idk::assets::AssetType;

pub type Assets = idk::assets::Assets<Gfx, Sheet, Ttf>;
pub type Audio = idk::audio::Audio<Bgm, Sfx>;

#[derive(Debug, Clone, Copy, Enum, strum::Display, strum::EnumIter)]
#[strum(serialize_all = "snake_case")]
pub enum Gfx {
    TownBg,
    UpgradeBg,
    QuestBg,
    ResolutionBg,
    ContinueButton,
    IncompleteButton,
    Redd,
    ReddTired,
    Violet,
    VioletTired,
    Gene,
    GeneTired,
    Yelli,
    YelliTired,
    Map,
    Title,
}

#[derive(Debug, Clone, Copy, Enum, strum::Display, strum::EnumIter)]
#[strum(serialize_all = "snake_case")]
pub enum Sheet {}

#[derive(Debug, Clone, Copy, Enum, strum::Display, strum::EnumIter)]
pub enum Ttf {
    #[strum(serialize = "Hack-Bold")]
    Hack,
    #[strum(serialize = "UbuntuMono-B")]
    UbuntuMono,
    #[strum(serialize = "Cuprum-Regular")]
    Cuprum,
    #[strum(serialize = "munro")]
    Munro,
}

#[derive(Debug, Clone, Copy, Enum, strum::Display, strum::EnumIter)]
#[strum(serialize_all = "snake_case")]
pub enum Bgm {
    TimeCrunch,
}

#[derive(Debug, Clone, Copy, Enum, strum::Display, strum::EnumIter)]
#[strum(serialize_all = "snake_case")]
pub enum Sfx {
    Click,
    Failure,
    Success,
}

impl AssetType for Gfx {}
impl AssetType for Sheet {}
impl AssetType for Ttf {}
impl AssetType for Bgm {}
impl AssetType for Sfx {}
