#![cfg_attr(rustfmt, rustfmt_skip)]

use macroquad::prelude::*;

use crate::{building_menu::Building, Context, assets::Ttf};

const TOWN_DESC_TOP_LEFT: Vec2 = vec2(50.0, 380.0);

pub fn draw_building_description(ctx: &Context, building: Building) {
    draw_rectangle(38.0, 356.0, 311.0, 234.0, BLACK);
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };
    match building {
        Building::Inn => {
            draw_text_ex(&format!("Inn - Level: {}", ctx.data.building_levels[Building::Inn].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Houses adventurers.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            draw_text_ex("The village must have an inn.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
        },
        Building::Blacksmith => {
            draw_text_ex(&format!("Blacksmith - Level: {}", ctx.data.building_levels[Building::Blacksmith].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Forges weapons and armor.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
        },
        Building::Academy => {
            draw_text_ex(&format!("Academy - Level: {}", ctx.data.building_levels[Building::Academy].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Researches new skill and magic.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
        },
        Building::Bakery => {
            draw_text_ex(&format!("Bakery - Level: {}", ctx.data.building_levels[Building::Bakery].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Sells baked goods.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
        },
        Building::PotionShop => {
            draw_text_ex(&format!("Potion Shop - Level: {}", ctx.data.building_levels[Building::PotionShop].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Sells high powered consumables.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
        },
        Building::Park => {
            draw_text_ex(&format!("Park - Level: {}", ctx.data.building_levels[Building::Park].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("A place for adventurers to recover", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            draw_text_ex("from fatigue.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
        },
        Building::Library => {
            draw_text_ex(&format!("Library - Level: {}", ctx.data.building_levels[Building::Library].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Adventurers can gain experience by", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            draw_text_ex("studying here.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
        },
        Building::Mine => {
            draw_text_ex(&format!("Mine - Level: {}", ctx.data.building_levels[Building::Mine].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            draw_text_ex("Mine for extra gold.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
        },
    }
}

pub fn draw_town_text(ctx: &Context) {
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };
    draw_text_ex("Select a building from the side menu.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
    draw_text_ex("Place building in the grid.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
    draw_text_ex("Place a building in the remove box", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
    draw_text_ex("to destroy it.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 75.0, params.clone());
    draw_text_ex("Mouse wheel rotates buildings.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 100.0, params.clone());
    draw_text_ex("Spacebar flips buildings.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 125.0, params.clone());
    draw_text_ex("The price must be within budget.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 150.0, params.clone());
    draw_text_ex("There must be an Inn.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 175.0, params.clone());
}

pub fn draw_upgrade_text(ctx: &Context) {
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };
    draw_text_ex("Click on a building to upgrade it.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
}

pub fn draw_upgrade_description(ctx: &Context, building: Building) {
    draw_rectangle(38.0, 356.0, 311.0, 234.0, BLACK);
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };
    match building {
        Building::Inn => {
            draw_text_ex(&format!("Inn - Level: {}", ctx.data.building_levels[Building::Inn].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
        },
        Building::Blacksmith => {
            draw_text_ex(&format!("Blacksmith - Level: {}", ctx.data.building_levels[Building::Blacksmith].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            match ctx.data.building_levels[Building::Blacksmith].current + 1 {
                1 => draw_text_ex("Violet weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                2 => draw_text_ex("Redd weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                3 => {
                    draw_text_ex("Gene weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
                    draw_text_ex("Yelli weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
                },
                4 => draw_text_ex("Armor for full party.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                5 => {
                    draw_text_ex("Violet weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
                    draw_text_ex("Gene weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
                    draw_text_ex("Yelli weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 75.0, params.clone());
                },
                6 => draw_text_ex("Redd weapon level up.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                _ => {},
            }
        },
        Building::Academy => {
            draw_text_ex(&format!("Academy - Level: {}", ctx.data.building_levels[Building::Academy].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            match ctx.data.building_levels[Building::Academy].current + 1 {
                1 => {
                    draw_text_ex("Violet learns Ice Attack.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
                    draw_text_ex("Gene learns Ice Attack.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
                },
                2 => draw_text_ex("Yelli learns Electric Attack.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                3 => {
                    draw_text_ex("Gene learns Fire Attack.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
                    draw_text_ex("Yelli learns Barrier.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 50.0, params.clone());
                },
                4 => draw_text_ex("Yelli learns Revive.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                5 => draw_text_ex("Redd learns Fire Attack.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                6 => draw_text_ex("Gene learns Barrier.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                _ => {},
            }
        },
        Building::Bakery => {
            draw_text_ex(&format!("Bakery - Level: {}", ctx.data.building_levels[Building::Bakery].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            match ctx.data.building_levels[Building::Bakery].current + 1 {
                1 => draw_text_ex("Bread for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                2 => draw_text_ex("Cookies for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                _ => {},
            }
        },
        Building::PotionShop => {
            draw_text_ex(&format!("Potion Shop - Level: {}", ctx.data.building_levels[Building::PotionShop].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            match ctx.data.building_levels[Building::PotionShop].current + 1 {
                1 => draw_text_ex("Fire Res Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                2 => draw_text_ex("Electric Res Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                3 => draw_text_ex("Poison Res Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                4 => draw_text_ex("Ice Res Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                5 => draw_text_ex("Health Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                6 => draw_text_ex("Revive Potion for sale.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone()),
                _ => {},
            }        },
        Building::Park => {
            draw_text_ex(&format!("Park - Level: {}", ctx.data.building_levels[Building::Park].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            if ctx.data.building_levels[Building::Park].current < 3 {
                draw_text_ex("Increases fatigue recovery.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            }
        },
        Building::Library => {
            draw_text_ex(&format!("Library - Level: {}", ctx.data.building_levels[Building::Library].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            if ctx.data.building_levels[Building::Library].current < 3 {
                draw_text_ex("Increases experience gain.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            }
        },
        Building::Mine => {
            draw_text_ex(&format!("Mine - Level: {}", ctx.data.building_levels[Building::Mine].level_string()), TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y, params.clone());
            if ctx.data.building_levels[Building::Mine].current < 3 {
                draw_text_ex("Increases gold yeild.", TOWN_DESC_TOP_LEFT.x, TOWN_DESC_TOP_LEFT.y + 25.0, params.clone());
            }
        },
    }
}