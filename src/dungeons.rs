use enum_map::{enum_map, EnumMap};
use macroquad::{
    prelude::WHITE,
    text::{draw_text_ex, TextParams},
};

use crate::{
    adventurers::{Skill, DESC_BOX_TOP_LEFT},
    assets::Ttf,
    scenes::quest::Quest,
    Context,
};

pub struct Dungeon {
    pub level: usize,
    pub attributes: Vec<Attribute>,
    pub reward: usize,
    pub clear_count: usize,
    pub exploration_count: usize,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, strum::Display)]
pub enum Attribute {
    #[strum(serialize = "Enemies with high HP")]
    DamageSponge,
    #[strum(serialize = "Enemies can be outsped")]
    Slowpoke,
    #[strum(serialize = "Enemies resist physical attacks")]
    PhysicallyDefensive,
    #[strum(serialize = "Enemies with strong physical attacks")]
    HeavyHitter,
    #[strum(serialize = "Enemies use fire attacks")]
    FireAttacks,
    #[strum(serialize = "Enemies use ice attacks")]
    IceAttacks,
    #[strum(serialize = "Enemies use electric attacks")]
    ElectricAttacks,
    #[strum(serialize = "Enemies use poison attacks")]
    PoisonAttacks,
    #[strum(serialize = "Enemies weak to fire")]
    FireWeakness,
    #[strum(serialize = "Enemies weak to ice")]
    IceWeakness,
    #[strum(serialize = "Enemies weak to electricity")]
    ElectricWeakness,
    #[strum(serialize = "It is a long journey")]
    LongJourney,
}

impl Attribute {
    pub fn effect(&self, skills: &[Skill]) -> isize {
        match self {
            Attribute::DamageSponge => match skills.contains(&Skill::StrengthBoost) {
                true => 0,
                false => -15,
            },
            Attribute::Slowpoke => match skills.contains(&Skill::SpeedBoost) {
                true => 0,
                false => -15,
            },
            Attribute::PhysicallyDefensive => match skills.contains(&Skill::MagicBoost) {
                true => 0,
                false => -15,
            },
            Attribute::HeavyHitter => match skills.contains(&Skill::DefenseBoost) {
                true => 0,
                false => -15,
            },
            Attribute::FireAttacks => match skills.contains(&Skill::FireResistance) {
                true => 0,
                false => -20,
            },
            Attribute::IceAttacks => match skills.contains(&Skill::IceResistance) {
                true => 0,
                false => -20,
            },
            Attribute::ElectricAttacks => match skills.contains(&Skill::ElectricResistance) {
                true => 0,
                false => -20,
            },
            Attribute::PoisonAttacks => match skills.contains(&Skill::PoisonResistance) {
                true => 0,
                false => -20,
            },
            Attribute::FireWeakness => match skills.contains(&Skill::FireAttack) {
                true => 30,
                false => 0,
            },
            Attribute::IceWeakness => match skills.contains(&Skill::IceAttack) {
                true => 30,
                false => 0,
            },
            Attribute::ElectricWeakness => match skills.contains(&Skill::ElectricAttack) {
                true => 30,
                false => 0,
            },
            Attribute::LongJourney => match skills.contains(&Skill::Cookies) {
                true => 5,
                false => -5,
            },
        }
    }
}

pub fn load_dungeons() -> EnumMap<Quest, Dungeon> {
    enum_map! {
        Quest::Library => Dungeon {
            level: 0,
            attributes: Vec::new(),
            reward: 0,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Park => Dungeon {
            level: 0,
            attributes: Vec::new(),
            reward: 0,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Mine => Dungeon {
            level: 0,
            attributes: Vec::new(),
            reward: 0,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun1 => Dungeon {
            level: 3,
            attributes: vec![
                Attribute::FireWeakness,
                Attribute::ElectricWeakness,
            ],
            reward: 1000,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun2 => Dungeon {
            level: 5,
            attributes: vec![
                Attribute::FireAttacks,
                Attribute::IceWeakness,
                Attribute::Slowpoke,
            ],
            reward: 2000,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun3 => Dungeon {
            level: 8,
            attributes: vec![
                Attribute::PoisonAttacks,
                Attribute::FireWeakness,
                Attribute::HeavyHitter,
                Attribute::PhysicallyDefensive,
            ],
            reward: 3500,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun4 => Dungeon {
            level: 12,
            attributes: vec![
                Attribute::LongJourney,
                Attribute::IceAttacks,
                Attribute::PoisonAttacks,
                Attribute::FireWeakness,
                Attribute::DamageSponge,
            ],
            reward: 3500,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun5 => Dungeon {
            level: 15,
            attributes: vec![
                Attribute::ElectricAttacks,
                Attribute::IceWeakness,
                Attribute::PhysicallyDefensive,
                Attribute::DamageSponge,
                Attribute::LongJourney,
            ],
            reward: 4000,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Dun6 => Dungeon {
            level: 17,
            attributes: vec![
                Attribute::ElectricWeakness,
                Attribute::PoisonAttacks,
                Attribute::IceAttacks,
                Attribute::LongJourney,
                Attribute::Slowpoke,
            ],
            reward: 4000,
            clear_count: 0,
            exploration_count: 0,
        },
        Quest::Final => Dungeon {
            level: 20,
            attributes: vec![
                Attribute::FireAttacks,
                Attribute::IceAttacks,
                Attribute::ElectricAttacks,
                Attribute::PoisonAttacks,
                Attribute::LongJourney,
            ],
            reward: 10000,
            clear_count: 0,
            exploration_count: 0,
        },
    }
}

pub fn draw_dungeon_description(ctx: &Context, quest: Quest) {
    if quest == Quest::Park || quest == Quest::Mine || quest == Quest::Library {
        return;
    }

    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };

    draw_text_ex(
        &format!("Level: {}", ctx.data.dungeons[quest].level),
        DESC_BOX_TOP_LEFT.x,
        DESC_BOX_TOP_LEFT.y,
        params.clone(),
    );
    let mut offset = 25.0;
    for attribute in &ctx.data.dungeons[quest].attributes {
        draw_text_ex(
            &attribute.to_string(),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y + offset,
            params.clone(),
        );
        offset += 25.0;
    }
}
