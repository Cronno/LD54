use std::time::Duration;

use enum_map::{enum_map, EnumMap};
use idk::{
    input::{InputCollector, InputHandler, MouseInput},
    scene::{Scene, SceneAction},
    transition::WipeRight,
};
use macroquad::prelude::*;
use strum::IntoEnumIterator;

use super::{
    quest::{PartyMember, QuestScene},
    town::{draw_day, draw_money, PlacedBuilding, CONTINUE_BUTTON},
};
use crate::{
    adventurers::Skill,
    assets::{Gfx, Sfx},
    building_menu::Building,
    descriptions::{draw_upgrade_description, draw_upgrade_text},
    grid::{Grid, GridKind},
    Context, GameInput,
};

pub struct UpgradeScene {
    inputs: InputHandler<GameInput>,
    town_grid: Grid,
    placed_buildings: EnumMap<Building, Option<PlacedBuilding>>,
    hovered: Option<Building>,
}

impl Scene<Context> for UpgradeScene {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events);
    }

    fn update(&mut self, ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        let mouse_pos = self.inputs.mouse_pos();
        if self.inputs.mouse_pressed(MouseInput::Left) && self.town_grid.hovered(mouse_pos) {
            if let Some((col, row)) = self.town_grid.grid_index_at_point(mouse_pos) {
                for (name, placed) in &mut self.placed_buildings {
                    let Some(building) = placed else {
                        continue;
                    };
                    if building.is_at_position(col, row) && building.grid == GridKind::Town {
                        let cost = upgrade_price(ctx, name);
                        if ctx.data.gold >= cost {
                            ctx.audio.play(Sfx::Click);
                            upgrade_building(ctx, name);
                            ctx.data.gold -= cost;
                        }
                    }
                }
            }
        }

        self.hovered = None;
        if self.town_grid.hovered(mouse_pos) {
            if let Some((col, row)) = self.town_grid.grid_index_at_point(mouse_pos) {
                for (name, placed) in &mut self.placed_buildings {
                    let Some(building) = placed else {
                        continue;
                    };
                    if building.is_at_position(col, row) && building.grid == GridKind::Town {
                        self.hovered = Some(name);
                    }
                }
            }
        }

        if self.inputs.mouse_pressed(MouseInput::Left) && CONTINUE_BUTTON.contains(mouse_pos) {
            let buildings: Vec<Building> = self
                .placed_buildings
                .iter()
                .filter_map(|(name, place)| {
                    if place.is_some() && place.clone().unwrap().grid == GridKind::Town {
                        Some(name)
                    } else {
                        None
                    }
                })
                .collect();
            apply_upgrades(ctx, &buildings);
            ctx.audio.play(Sfx::Click);
            SceneAction::Replace(QuestScene::new(ctx, buildings), WipeRight::new(BLACK))
        } else {
            SceneAction::Continue
        }
    }

    fn render(&self, ctx: &Context) {
        draw_texture(ctx.assets.gfx(Gfx::UpgradeBg), 0.0, 0.0, WHITE);
        self.town_grid.draw();
        self.draw_buildings();
        draw_day(ctx);
        draw_upgrade_text(ctx);

        let mut cost = None;
        if let Some(building) = self.hovered {
            cost = Some(upgrade_price(ctx, building));
            draw_upgrade_description(ctx, building);
        }

        draw_money(ctx, cost);

        draw_texture(
            ctx.assets.gfx(Gfx::ContinueButton),
            CONTINUE_BUTTON.x,
            CONTINUE_BUTTON.y,
            WHITE,
        );
    }
}

impl UpgradeScene {
    pub fn new(
        ctx: &Context,
        town_grid: Grid,
        placed_buildings: EnumMap<Building, Option<PlacedBuilding>>,
    ) -> Box<Self> {
        Box::new(Self {
            inputs: InputHandler::new(&ctx.settings.game_input_config),
            town_grid,
            placed_buildings,
            hovered: None,
        })
    }

    fn draw_buildings(&self) {
        for (_, data) in self.placed_buildings.iter() {
            let Some(building) = data else {
                continue;
            };

            if building.grid == GridKind::Town {
                building.mino.draw_placed(building.col, building.row, &self.town_grid);
            }
        }
    }
}

pub struct BuildingLevel {
    pub current: usize,
    max: usize,
}

impl BuildingLevel {
    pub fn level_string(&self) -> String {
        if self.current == self.max {
            String::from("MAX")
        } else {
            self.current.to_string()
        }
    }
}

fn upgrade_building(ctx: &mut Context, building: Building) {
    ctx.data.building_levels[building].current = std::cmp::min(
        ctx.data.building_levels[building].current + 1,
        ctx.data.building_levels[building].max,
    );
}

pub fn load_building_levels() -> EnumMap<Building, BuildingLevel> {
    enum_map! {
        Building::Blacksmith => BuildingLevel { current: 1, max: 6 },
        Building::Academy => BuildingLevel { current: 1, max: 6 },
        Building::Bakery => BuildingLevel { current: 1, max: 2 },
        Building::Inn => BuildingLevel { current: 1, max: 1 },
        Building::Library => BuildingLevel { current: 1, max: 3 },
        Building::Mine => BuildingLevel { current: 1, max: 3 },
        Building::Park => BuildingLevel { current: 1, max: 3 },
        Building::PotionShop => BuildingLevel { current: 1, max: 6 },
    }
}

fn upgrade_price(ctx: &Context, building: Building) -> usize {
    match building {
        Building::Blacksmith => match ctx.data.building_levels[Building::Blacksmith].current {
            1 => 300,
            2 => 400,
            3 => 400,
            4 => 700,
            5 => 900,
            _ => 0,
        },
        Building::Inn => match ctx.data.building_levels[Building::Inn].current {
            1 => 0,
            _ => 0,
        },
        Building::Academy => match ctx.data.building_levels[Building::Academy].current {
            1 => 300,
            2 => 400,
            3 => 400,
            4 => 700,
            5 => 900,
            _ => 0,
        },
        Building::Bakery => match ctx.data.building_levels[Building::Bakery].current {
            1 => 200,
            _ => 0,
        },
        Building::PotionShop => match ctx.data.building_levels[Building::PotionShop].current {
            1 => 300,
            2 => 400,
            3 => 400,
            4 => 700,
            5 => 900,
            _ => 0,
        },
        Building::Park => match ctx.data.building_levels[Building::Park].current {
            1 => 300,
            2 => 500,
            _ => 0,
        },
        Building::Library => match ctx.data.building_levels[Building::Library].current {
            1 => 300,
            2 => 500,
            _ => 0,
        },
        Building::Mine => match ctx.data.building_levels[Building::Mine].current {
            1 => 300,
            2 => 500,
            _ => 0,
        },
    }
}

fn apply_upgrades(ctx: &mut Context, buildings: &[Building]) {
    if buildings.contains(&Building::Blacksmith) {
        match ctx.data.building_levels[Building::Blacksmith].current {
            1 => {
                ctx.data.party[PartyMember::Violet].weapon_impact = 7;
            }
            2 => {
                ctx.data.party[PartyMember::Redd].weapon_impact = 9;
            }
            3 => {
                ctx.data.party[PartyMember::Gene].weapon_impact = 6;
                ctx.data.party[PartyMember::Yelli].weapon_impact = 8;
            }
            4 => {
                for character in PartyMember::iter() {
                    ctx.data.party[character].add_skill(Skill::Armor);
                }
            }
            5 => {
                ctx.data.party[PartyMember::Violet].weapon_impact = 9;
                ctx.data.party[PartyMember::Gene].weapon_impact = 7;
                ctx.data.party[PartyMember::Yelli].weapon_impact = 10;
            }
            6 => {
                ctx.data.party[PartyMember::Redd].weapon_impact = 12;
            }
            _ => {}
        }
    }

    if buildings.contains(&Building::Academy) {
        match ctx.data.building_levels[Building::Academy].current {
            1 => {
                ctx.data.party[PartyMember::Violet].add_skill(Skill::IceAttack);
                ctx.data.party[PartyMember::Gene].add_skill(Skill::IceAttack);
            }
            2 => {
                ctx.data.party[PartyMember::Yelli].add_skill(Skill::ElectricAttack);
            }
            3 => {
                ctx.data.party[PartyMember::Gene].add_skill(Skill::FireAttack);
                ctx.data.party[PartyMember::Yelli].add_skill(Skill::Barrier);
            }
            4 => {
                ctx.data.party[PartyMember::Yelli].add_skill(Skill::Revive);
            }
            5 => {
                ctx.data.party[PartyMember::Redd].add_skill(Skill::FireAttack);
            }
            6 => {
                ctx.data.party[PartyMember::Gene].add_skill(Skill::Barrier);
            }
            _ => {}
        }
    }

    if buildings.contains(&Building::Bakery) {
        let mut bakery_items = Vec::new();
        if ctx.data.building_levels[Building::Bakery].current >= 1 {
            bakery_items.push(Skill::Bread);
        }
        if ctx.data.building_levels[Building::Bakery].current >= 2 {
            bakery_items.push(Skill::Cookies);
        }
        for skill in bakery_items.iter() {
            for character in PartyMember::iter() {
                ctx.data.party[character].add_skill(*skill);
            }
        }
    }

    if buildings.contains(&Building::PotionShop) {
        let mut potions = Vec::new();
        if ctx.data.building_levels[Building::PotionShop].current >= 1 {
            potions.push(Skill::FireResistance);
        }
        if ctx.data.building_levels[Building::PotionShop].current >= 2 {
            potions.push(Skill::ElectricResistance);
        }
        if ctx.data.building_levels[Building::PotionShop].current >= 3 {
            potions.push(Skill::PoisonResistance);
        }
        if ctx.data.building_levels[Building::PotionShop].current >= 4 {
            potions.push(Skill::IceResistance);
        }
        if ctx.data.building_levels[Building::PotionShop].current >= 5 {
            potions.push(Skill::HealthPotion);
        }
        if ctx.data.building_levels[Building::PotionShop].current >= 6 {
            potions.push(Skill::Revive);
        }
        for skill in potions.iter() {
            for character in PartyMember::iter() {
                ctx.data.party[character].add_skill(*skill);
            }
        }
    }
}
