use std::time::Duration;

use idk::{
    input::InputCollector,
    scene::{Scene, SceneAction},
};
use macroquad::prelude::*;

use crate::{assets::Ttf, Context};

pub struct EndScene {
    success: bool,
}

impl Scene<Context> for EndScene {
    fn handle_input(&mut self, _input_events: &InputCollector) {}

    fn update(&mut self, _ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        SceneAction::Continue
    }

    fn render(&self, ctx: &Context) {
        let params = TextParams {
            font: ctx.assets.fonts(Ttf::Munro),
            font_size: 72,
            color: WHITE,
            ..Default::default()
        };

        if self.success {
            draw_text_ex("Victory", 600.0, 100.0, params.clone());
            draw_text_ex("Thanks for playing", 600.0, 400.0, params.clone());
        } else {
            draw_text_ex("Game Over", 600.0, 100.0, params.clone());
            draw_text_ex("Thanks for playing", 600.0, 400.0, params.clone());
        }
    }
}

impl EndScene {
    pub fn new(success: bool) -> Box<Self> {
        Box::new(Self { success })
    }
}
