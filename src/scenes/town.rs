use std::{collections::HashSet, time::Duration};

use enum_map::EnumMap;
use idk::{
    input::{InputCollector, InputHandler, MouseInput, MouseWheel},
    scene::{Scene, SceneAction},
    transition::WipeRight,
};
use macroquad::prelude::*;

use super::upgrade::UpgradeScene;
use crate::{
    assets::{Gfx, Ttf, Sfx},
    building_menu::{building_cost, Building, BuildingMenu},
    descriptions::draw_town_text,
    grid::{Grid, GridKind},
    polymino::Polymino,
    settings::Settings,
    Context, GameInput,
};

pub const TILE_SIZE: f32 = 49.0;
pub const TOWN_TOP_LEFT: Vec2 = vec2(415.0, 105.0);
pub const DESTROY_TOP_LEFT: Vec2 = vec2(104.0, 70.0);
pub const GRID_WIDTH: usize = 10;
pub const GRID_HEIGHT: usize = 10;
pub const CONTINUE_BUTTON: Rect = Rect {
    x: 920.0,
    y: 620.0,
    w: 380.0,
    h: 130.0,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TownElement {
    Empty,
    Wall,
    Building,
    Outside,
}

impl Default for TownElement {
    fn default() -> Self {
        Self::Outside
    }
}

#[derive(Debug, Clone)]
pub struct PlacedBuilding {
    pub mino: Polymino,
    pub col: usize,
    pub row: usize,
    pub grid: GridKind,
}

impl PlacedBuilding {
    pub fn is_at_position(&self, col: usize, row: usize) -> bool {
        for index in self.indices() {
            if index.0 == col && index.1 == row {
                return true;
            }
        }
        false
    }

    pub fn indices(&self) -> Vec<(usize, usize)> {
        let mut indices = Vec::new();
        for (rel_col, rel_row) in &self.mino.shape {
            let col = (rel_col + self.col as isize) as usize;
            let row = (rel_row + self.row as isize) as usize;
            indices.push((col, row))
        }
        indices
    }
}

pub struct TownScene {
    side_menu: BuildingMenu,

    inputs: InputHandler<GameInput>,
    town_grid: Grid,
    destroy_grid: Grid,
    mouse_holding: Option<Polymino>,
    placed_buildings: EnumMap<Building, Option<PlacedBuilding>>,
    previous_buildings: HashSet<Building>,
}

impl TownScene {
    pub fn new(settings: &Settings) -> Box<Self> {
        let mut town_matrix = vec![vec![TownElement::Outside; GRID_HEIGHT]; GRID_WIDTH];
        town_matrix[3][1] = TownElement::Empty;
        town_matrix[4][1] = TownElement::Empty;
        town_matrix[2][2] = TownElement::Empty;
        town_matrix[3][2] = TownElement::Empty;
        town_matrix[4][2] = TownElement::Empty;
        town_matrix[6][2] = TownElement::Empty;
        town_matrix[4][3] = TownElement::Empty;
        town_matrix[5][3] = TownElement::Empty;
        town_matrix[6][3] = TownElement::Empty;
        town_matrix[2][4] = TownElement::Empty;
        town_matrix[3][4] = TownElement::Empty;
        town_matrix[4][4] = TownElement::Empty;
        town_matrix[5][4] = TownElement::Empty;
        town_matrix[1][5] = TownElement::Empty;
        town_matrix[2][5] = TownElement::Empty;
        town_matrix[3][5] = TownElement::Empty;
        town_matrix[4][5] = TownElement::Empty;
        town_matrix[5][5] = TownElement::Empty;
        town_matrix[6][5] = TownElement::Empty;
        town_matrix[7][5] = TownElement::Empty;
        town_matrix[3][6] = TownElement::Empty;
        town_matrix[4][6] = TownElement::Empty;
        town_matrix[6][6] = TownElement::Empty;
        town_matrix[3][7] = TownElement::Empty;
        town_matrix[4][7] = TownElement::Empty;
        town_matrix[6][7] = TownElement::Empty;
        town_matrix[3][8] = TownElement::Empty;
        set_walls(&mut town_matrix);

        let destroy_matrix = vec![vec![TownElement::Empty; 5]; 5];

        let previous_buildings = HashSet::new();

        Box::new(Self {
            side_menu: BuildingMenu::new(),
            inputs: InputHandler::new(&settings.game_input_config),
            town_grid: Grid::new_town(town_matrix),
            destroy_grid: Grid::new_destroy(destroy_matrix),
            mouse_holding: None,
            placed_buildings: EnumMap::default(),
            previous_buildings,
        })
    }

    fn calculate_price(&self) -> usize {
        let mut current_buildings = HashSet::new();
        for (name, place) in self.placed_buildings.iter() {
            if let Some(placed_building) = place {
                if placed_building.grid == GridKind::Town {
                    current_buildings.insert(name);
                }
            }
        }

        let mut price = 0;
        for building in current_buildings.difference(&self.previous_buildings) {
            price += building_cost(*building);
        }

        price
    }

    fn draw_buildings(&self) {
        for (_, data) in self.placed_buildings.iter() {
            let Some(building) = data else {
                continue;
            };

            if building.grid == GridKind::Town {
                building.mino.draw_placed(building.col, building.row, &self.town_grid);
            } else if building.grid == GridKind::Destroy {
                building
                    .mino
                    .draw_placed(building.col, building.row, &self.destroy_grid);
            }
        }
    }

    fn scene_exit_checks(&self, ctx: &Context) -> bool {
        let Some(inn_place) = &self.placed_buildings[Building::Inn] else {
            return false;
        };

        if inn_place.grid != GridKind::Town {
            return false;
        }

        if ctx.data.gold < self.calculate_price() {
            return false;
        }

        true
    }
}

impl Scene<Context> for TownScene {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events);
    }

    fn update(&mut self, ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        let mouse_pos = self.inputs.mouse_pos();
        if mouse_pos.x > 900.0 && self.inputs.mouse_pressed(MouseInput::Left) {
            self.mouse_holding = match self.side_menu.click(mouse_pos) {
                Some(Building::Inn) => Some(Polymino::inn()),
                Some(Building::Blacksmith) => Some(Polymino::blacksmith()),
                Some(Building::Academy) => Some(Polymino::academy()),
                Some(Building::Park) => Some(Polymino::park()),
                Some(Building::Library) => Some(Polymino::library()),
                Some(Building::PotionShop) => Some(Polymino::potion_shop()),
                Some(Building::Mine) => Some(Polymino::mine()),
                Some(Building::Bakery) => Some(Polymino::bakery()),
                None => self.mouse_holding.clone(),
            };
        }

        let mut empty_held = false;
        if let Some(mino) = &mut self.mouse_holding {
            if self.inputs.mouse_wheel_rolled(MouseWheel::Up) {
                mino.rotate_cw();
            }
            if self.inputs.mouse_wheel_rolled(MouseWheel::Down) {
                mino.rotate_ccw();
            }
            if self.inputs.mouse_pressed(MouseInput::Right) || self.inputs.pressed(GameInput::Mirror) {
                mino.mirror();
            }
            self.town_grid.update_ghost_piece(mouse_pos);
            self.destroy_grid.update_ghost_piece(mouse_pos);
            if self.town_grid.hovered(mouse_pos) && self.inputs.mouse_pressed(MouseInput::Left) {
                ctx.audio.play(Sfx::Click);
                if let Some(building) = self.town_grid.set_building(mino) {
                    self.placed_buildings[mino.kind] = Some(building);
                    empty_held = true;
                }
            }
            if self.destroy_grid.hovered(mouse_pos) && self.inputs.mouse_pressed(MouseInput::Left) {
                ctx.audio.play(Sfx::Click);
                if let Some(building) = self.destroy_grid.set_building(mino) {
                    self.placed_buildings[mino.kind] = Some(building);
                    empty_held = true;
                }
            }
        } else if self.inputs.mouse_pressed(MouseInput::Left) {
            if self.town_grid.hovered(mouse_pos) {
                if let Some((col, row)) = self.town_grid.grid_index_at_point(mouse_pos) {
                    for (_, placed) in &mut self.placed_buildings {
                        let Some(building) = placed else {
                            continue;
                        };
                        if building.is_at_position(col, row) && building.grid == GridKind::Town {
                            ctx.audio.play(Sfx::Click);
                            self.mouse_holding = Some(building.mino.clone());
                            self.town_grid.remove(building);
                            *placed = None;
                        }
                    }
                }
            } else if self.destroy_grid.hovered(mouse_pos) {
                if let Some((col, row)) = self.destroy_grid.grid_index_at_point(mouse_pos) {
                    for (_, placed) in &mut self.placed_buildings {
                        let Some(building) = placed else {
                            continue;
                        };
                        if building.is_at_position(col, row) && building.grid == GridKind::Destroy {
                            ctx.audio.play(Sfx::Click);
                            self.mouse_holding = Some(building.mino.clone());
                            self.destroy_grid.remove(building);
                            *placed = None;
                        }
                    }
                }
            }
        }

        if empty_held {
            self.mouse_holding = None;
        }

        if self.inputs.pressed(GameInput::Exit) {
            self.mouse_holding = None;
        }

        self.side_menu.update(&self.placed_buildings);

        if self.inputs.mouse_pressed(MouseInput::Left)
            && CONTINUE_BUTTON.contains(mouse_pos)
            && self.scene_exit_checks(ctx)
        {
            ctx.audio.play(Sfx::Click);
            ctx.data.gold = ctx.data.gold.saturating_sub(self.calculate_price());
            self.previous_buildings.clear();
            for (name, place) in self.placed_buildings.iter_mut() {
                if let Some(thing) = place {
                    if thing.grid == GridKind::Town {
                        self.previous_buildings.insert(name);
                    } else if thing.grid == GridKind::Destroy {
                        self.destroy_grid.remove(thing);
                        *place = None;
                    }
                }
            }

            SceneAction::Push(
                UpgradeScene::new(ctx, self.town_grid.clone(), self.placed_buildings.clone()),
                WipeRight::new(BLACK),
            )
        } else {
            SceneAction::Continue
        }
    }

    fn render(&self, ctx: &Context) {
        draw_texture(ctx.assets.gfx(Gfx::TownBg), 0.0, 0.0, WHITE);
        self.town_grid.draw();
        self.destroy_grid.draw();
        self.draw_buildings();
        draw_town_text(ctx);

        self.side_menu.draw(ctx, self.inputs.mouse_pos());
        if self.scene_exit_checks(ctx) {
            draw_texture(
                ctx.assets.gfx(Gfx::ContinueButton),
                CONTINUE_BUTTON.x,
                CONTINUE_BUTTON.y,
                WHITE,
            );
        } else {
            draw_texture(
                ctx.assets.gfx(Gfx::IncompleteButton),
                CONTINUE_BUTTON.x,
                CONTINUE_BUTTON.y,
                WHITE,
            );
        }
        draw_money(ctx, Some(self.calculate_price()));
        draw_day(ctx);

        if let Some(mino) = &self.mouse_holding {
            if let Some(ghost) = self.town_grid.ghost_position {
                mino.draw_ghost(self.town_grid.grid_index_to_screen_coords(ghost.0, ghost.1));
            } else if let Some(ghost) = self.destroy_grid.ghost_position {
                mino.draw_ghost(self.destroy_grid.grid_index_to_screen_coords(ghost.0, ghost.1));
            }
            let mouse_pos = self.inputs.mouse_pos();
            mino.draw(mouse_pos);
        }
    }
}

fn set_walls(town_grid: &mut [Vec<TownElement>]) {
    let mut walls = Vec::new();
    for (col_i, col) in town_grid.iter().enumerate() {
        for (row_i, element) in col.iter().enumerate() {
            let checked_types = [TownElement::Empty, TownElement::Building];
            if checked_types.contains(element) {
                continue;
            }
            let mut neighbors = vec![(col_i, row_i + 1), (col_i + 1, row_i), (col_i + 1, row_i + 1)];
            if col_i != 0 {
                neighbors.push((col_i - 1, row_i));
                neighbors.push((col_i - 1, row_i + 1));
            }
            if row_i != 0 {
                neighbors.push((col_i, row_i - 1));
                neighbors.push((col_i + 1, row_i - 1));
            }
            if row_i != 0 && col_i != 0 {
                neighbors.push((col_i - 1, row_i - 1));
            }
            for (neighbor_col, neighbor_row) in neighbors {
                if town_grid.get(neighbor_col).is_some() && town_grid[neighbor_col].get(neighbor_row).is_some() {
                    let element = town_grid[neighbor_col][neighbor_row];
                    if checked_types.contains(&element) {
                        walls.push((col_i, row_i));
                    }
                }
            }
        }
    }
    for (col, row) in walls {
        town_grid[col][row] = TownElement::Wall;
    }
}

pub fn draw_money(ctx: &Context, cost: Option<usize>) {
    let mut params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 32,
        color: WHITE,
        ..Default::default()
    };

    draw_text_ex(&format!("Gold {:>5}", ctx.data.gold), 750.0, 50.0, params.clone());

    if let Some(cost) = cost {
        params.color = RED;
        draw_text_ex(&format!("-{:>5}", cost), 795.0, 75.0, params.clone());
    }
}

pub fn draw_day(ctx: &Context) {
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 32,
        color: WHITE,
        ..Default::default()
    };

    draw_text_ex(&format!("Day {} of 15", ctx.data.day), 10.0, 50.0, params);
}
