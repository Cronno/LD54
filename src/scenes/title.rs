use std::time::Duration;

use idk::{
    input::{InputCollector, InputHandler},
    scene::{Scene, SceneAction},
    transition::WipeRight,
};
use macroquad::prelude::*;

use super::town::{TownScene, CONTINUE_BUTTON};
use crate::{assets::{Gfx, Sfx, Bgm}, Context, GameInput};

pub struct TitleScene {
    inputs: InputHandler<GameInput>,
}

impl Scene<Context> for TitleScene {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events)
    }

    fn update(&mut self, ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        if CONTINUE_BUTTON.contains(self.inputs.mouse_pos()) && self.inputs.mouse_pressed(idk::input::MouseInput::Left)
        {
            ctx.audio.play(Sfx::Click);
            ctx.audio.start(Bgm::TimeCrunch);
            SceneAction::Push(TownScene::new(&ctx.settings), WipeRight::new(BLACK))
        } else {
            SceneAction::Continue
        }
    }

    fn render(&self, ctx: &Context) {
        draw_texture(ctx.assets.gfx(Gfx::Title), 0.0, 0.0, WHITE);
        draw_texture(
            ctx.assets.gfx(Gfx::ContinueButton),
            CONTINUE_BUTTON.x,
            CONTINUE_BUTTON.y,
            WHITE,
        );
    }
}

impl TitleScene {
    pub fn new(ctx: &Context) -> Box<Self> {
        Box::new(Self {
            inputs: InputHandler::new(&ctx.settings.game_input_config),
        })
    }
}
