use std::time::Duration;

use enum_map::{Enum, EnumMap};
use idk::{
    input::{InputCollector, InputHandler, MouseInput},
    menu::menu_tile::MenuTile,
    rect::draw_rect_lines,
    scene::{Scene, SceneAction},
    transition::WipeRight,
};
use macroquad::prelude::*;
use serde::{Deserialize, Serialize};

use super::{resolution::ResolutionScene, town::CONTINUE_BUTTON};
use crate::{
    adventurers::{draw_adventurer, draw_character_desc},
    assets::{Gfx, Ttf, Sfx},
    building_menu::Building,
    dungeons::draw_dungeon_description,
    Context, GameInput,
};

#[derive(Debug, Clone, Copy, Enum, PartialEq, Eq, Serialize, Deserialize, strum::Display, Default)]
pub enum Quest {
    #[default]
    #[strum(serialize = "Relax in Park")]
    Park,
    #[strum(serialize = "Study in Library")]
    Library,
    #[strum(serialize = "Mine in Mine")]
    Mine,
    #[strum(serialize = "Dark Cave")]
    Dun1,
    #[strum(serialize = "Spicy Volcano")]
    Dun2,
    #[strum(serialize = "Corrupt Ruins")]
    Dun3,
    #[strum(serialize = "Frostbitten Forest")]
    Dun4,
    #[strum(serialize = "Thunder Mountain")]
    Dun5,
    #[strum(serialize = "Ocean Bed Temple")]
    Dun6,
    #[strum(serialize = "Darkest Abyss")]
    Final,
}

#[derive(Debug, Clone, Copy, Enum, PartialEq, Eq, Serialize, Deserialize, strum::Display, strum::EnumIter)]
pub enum PartyMember {
    Redd,
    Violet,
    Gene,
    Yelli,
}

enum State {
    Party,
    Quest(PartyMember),
}

pub struct QuestScene {
    inputs: InputHandler<GameInput>,
    state: State,
    party_tiles: EnumMap<PartyMember, MenuTile<PartyMember>>,
    quest_tiles: EnumMap<Quest, MenuTile<Quest>>,
    quest_assignments: EnumMap<PartyMember, Option<Quest>>,
    buildings: Vec<Building>,
}

impl Scene<Context> for QuestScene {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events);
    }

    fn update(&mut self, ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        match self.state {
            State::Party => {
                if self.inputs.mouse_pressed(MouseInput::Left) {
                    let mouse_pos = self.inputs.mouse_pos();
                    for (name, tile) in self.party_tiles.iter() {
                        if tile.area.contains(mouse_pos) {
                            ctx.audio.play(Sfx::Click);
                            self.state = State::Quest(name);
                        }
                    }

                    if CONTINUE_BUTTON.contains(mouse_pos) && self.all_assigned() {
                        ctx.audio.play(Sfx::Click);

                        return SceneAction::Replace(
                            ResolutionScene::new(ctx, &self.quest_assignments),
                            WipeRight::new(BLACK),
                        );
                    }
                }
            }
            State::Quest(hero) => {
                if self.inputs.mouse_pressed(MouseInput::Left) {
                    let mouse_pos = self.inputs.mouse_pos();
                    if ctx.data.day < 15 {
                        for (name, tile) in self.quest_tiles.iter() {
                            if name == Quest::Park && !self.buildings.contains(&Building::Park) {
                                continue;
                            }
                            if name == Quest::Library && !self.buildings.contains(&Building::Library) {
                                continue;
                            }
                            if name == Quest::Mine && !self.buildings.contains(&Building::Mine) {
                                continue;
                            }
                            if tile.area.contains(mouse_pos) {
                                ctx.audio.play(Sfx::Click);
                                self.quest_assignments[hero] = Some(name);
                                self.state = State::Party;
                            }
                        }
                    } else if self.quest_tiles[Quest::Park].area.contains(mouse_pos) {
                        ctx.audio.play(Sfx::Click);
                        self.quest_assignments[hero] = Some(Quest::Final);
                        self.state = State::Party;
                    }
                }
            }
        }

        SceneAction::Continue
    }

    fn render(&self, ctx: &Context) {
        draw_texture(ctx.assets.gfx(Gfx::QuestBg), 0.0, 0.0, WHITE);
        draw_texture(ctx.assets.gfx(Gfx::Map), 27.0, 32.0, WHITE);

        let mut params = TextParams {
            font: ctx.assets.fonts(Ttf::Munro),
            font_size: 32,
            color: WHITE,
            ..Default::default()
        };

        match self.state {
            State::Party => {
                for (name, tile) in &self.party_tiles {
                    if tile.area.contains(self.inputs.mouse_pos()) {
                        draw_rect_lines(&tile.area, 5.0, WHITE);
                        draw_character_desc(ctx, name);
                        draw_adventurer(ctx, name, 590.0, 32.0)
                    }
                    draw_text_ex(
                        &name.to_string(),
                        tile.area.x + 40.0,
                        tile.area.y + 40.0,
                        params.clone(),
                    );
                    if let Some(quest) = self.quest_assignments[name] {
                        draw_text_ex(
                            &quest.to_string(),
                            tile.area.x + 40.0,
                            tile.area.y + 70.0,
                            params.clone(),
                        );
                    }
                }

                if self.all_assigned() {
                    draw_texture(
                        ctx.assets.gfx(Gfx::ContinueButton),
                        CONTINUE_BUTTON.x,
                        CONTINUE_BUTTON.y,
                        WHITE,
                    );
                } else {
                    draw_texture(
                        ctx.assets.gfx(Gfx::IncompleteButton),
                        CONTINUE_BUTTON.x,
                        CONTINUE_BUTTON.y,
                        WHITE,
                    );
                }
            }
            State::Quest(_) => {
                if ctx.data.day < 15 {
                    for (name, tile) in &self.quest_tiles {
                        if tile.area.contains(self.inputs.mouse_pos()) {
                            draw_rect_lines(&tile.area, 5.0, WHITE);
                            draw_dungeon_description(ctx, name);
                        }
                        params.color = if name == Quest::Park && !self.buildings.contains(&Building::Park)
                            || name == Quest::Library && !self.buildings.contains(&Building::Library)
                            || name == Quest::Mine && !self.buildings.contains(&Building::Mine)
                        {
                            GRAY
                        } else {
                            WHITE
                        };
                        draw_text_ex(
                            &name.to_string(),
                            tile.area.x + 40.0,
                            tile.area.y + 40.0,
                            params.clone(),
                        )
                    }
                } else {
                    let tile = &self.quest_tiles[Quest::Park];
                    if tile.area.contains(self.inputs.mouse_pos()) {
                        draw_rect_lines(&tile.area, 5.0, WHITE);
                        draw_dungeon_description(ctx, Quest::Final);
                    }
                    draw_text_ex(
                        &(Quest::Final).to_string(),
                        tile.area.x + 40.0,
                        tile.area.y + 40.0,
                        params.clone(),
                    )
                }
            }
        }
    }
}

impl QuestScene {
    pub fn new(ctx: &Context, buildings: Vec<Building>) -> Box<Self> {
        let party_tiles: EnumMap<PartyMember, MenuTile<PartyMember>> =
            ron::de::from_bytes(include_bytes!(r"../../menus/party_menu.ron")).unwrap();
        let quest_tiles: EnumMap<Quest, MenuTile<Quest>> =
            ron::de::from_bytes(include_bytes!(r"../../menus/quest_menu.ron")).unwrap();

        Box::new(Self {
            inputs: InputHandler::new(&ctx.settings.game_input_config),
            state: State::Party,
            party_tiles,
            quest_tiles,
            quest_assignments: EnumMap::default(),
            buildings,
        })
    }

    fn all_assigned(&self) -> bool {
        self.quest_assignments.iter().all(|(_, quest)| quest.is_some())
    }
}
