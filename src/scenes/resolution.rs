use std::time::Duration;

use enum_map::EnumMap;
use idk::{
    input::{InputCollector, InputHandler, MouseInput},
    scene::{Scene, SceneAction},
    transition::WipeRight,
};
use macroquad::prelude::*;

use super::{
    ending::EndScene,
    quest::{PartyMember, Quest},
    town::{draw_day, CONTINUE_BUTTON},
};
use crate::{
    adventurers::{draw_adventurer, Adventurer, Skill},
    assets::{Gfx, Ttf, Sfx},
    building_menu::Building,
    dungeons::Dungeon,
    Context, GameInput,
};

pub struct ResolutionScene {
    inputs: InputHandler<GameInput>,
    resolutions: EnumMap<PartyMember, Resolution>,
    current_character: PartyMember,
    current_level: usize,
    next_level: usize,
    exit: bool,
}

impl Scene<Context> for ResolutionScene {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events);
    }

    fn update(&mut self, ctx: &mut Context, _delta: Duration) -> SceneAction<Context> {
        if self.inputs.mouse_pressed(MouseInput::Left) {
            let mouse_pos = self.inputs.mouse_pos();
            if CONTINUE_BUTTON.contains(mouse_pos) {

                self.next_character();
                if self.resolutions[self.current_character].success {
                    ctx.audio.play(Sfx::Success);
                } else {
                    ctx.audio.play(Sfx::Failure);
                }
            }
        }

        let resolution = &self.resolutions[self.current_character];
        self.current_level = ctx.data.party[self.current_character].level();
        self.next_level = (ctx.data.party[self.current_character].experience + resolution.experience) / 100 + 1;

        if self.exit && ctx.data.day < 15 {
            self.apply_resolutions(ctx);
            ctx.data.day += 1;
            SceneAction::Pop(WipeRight::new(BLACK))
        } else if self.exit && ctx.data.day >= 15 {
            self.apply_resolutions(ctx);
            let success = self.resolutions.iter().map(|(_, res)| res.success).any(|s| s);
            SceneAction::Push(EndScene::new(success), WipeRight::new(BLACK))
        } else {
            SceneAction::Continue
        }
    }

    fn render(&self, ctx: &Context) {
        draw_texture(ctx.assets.gfx(Gfx::ResolutionBg), 0.0, 0.0, WHITE);
        draw_adventurer(ctx, self.current_character, 550.0, 22.0);

        let params = TextParams {
            font: ctx.assets.fonts(Ttf::Munro),
            font_size: 40,
            color: WHITE,
            ..Default::default()
        };
        draw_text_ex(&self.current_character.to_string(), 260.0, 230.0, params.clone());
        draw_day(ctx);

        let resolution = &self.resolutions[self.current_character];
        let text = if resolution.success { "Success" } else { "Failure" };
        draw_text_ex(text, 350.0, 310.0, params.clone());
        draw_text_ex(&format!("Reward: {}", resolution.reward), 350.0, 360.0, params.clone());
        draw_text_ex(&format!("Exp: {}", resolution.experience), 350.0, 410.0, params.clone());

        if self.current_level < self.next_level {
            draw_text_ex(
                &format!("Level Up: {} -> {}", self.current_level, self.next_level),
                350.0,
                460.0,
                params.clone(),
            );
        }

        if ctx.settings.show_debug {
            draw_text_ex(&format!("Chance: {}", resolution.chance), 350.0, 510.0, params.clone());
            draw_text_ex(
                &format!("Fatigue Impact: {}", resolution.fatigue_impact),
                350.0,
                560.0,
                params.clone(),
            );
            draw_text_ex(
                &format!("Fatigue: {}", ctx.data.party[self.current_character].fatigue),
                350.0,
                600.0,
                params.clone(),
            );
        }

        draw_texture(
            ctx.assets.gfx(Gfx::ContinueButton),
            CONTINUE_BUTTON.x,
            CONTINUE_BUTTON.y,
            WHITE,
        );
    }
}

impl ResolutionScene {
    pub fn new(ctx: &mut Context, quest_assignments: &EnumMap<PartyMember, Option<Quest>>) -> Box<Self> {
        let resolutions = Self::generate_results(ctx, quest_assignments);

        if resolutions[PartyMember::Redd].success {
            ctx.audio.play(Sfx::Success);
        } else {
            ctx.audio.play(Sfx::Failure);
        }

        Box::new(Self {
            inputs: InputHandler::new(&ctx.settings.game_input_config),
            resolutions,
            current_character: PartyMember::Redd,
            current_level: 0,
            next_level: 0,
            exit: false,
        })
    }

    fn generate_results(
        ctx: &Context,
        quest_assignments: &EnumMap<PartyMember, Option<Quest>>,
    ) -> EnumMap<PartyMember, Resolution> {
        let mut resolutions = EnumMap::default();
        for (name, quest) in quest_assignments.iter() {
            let adventurer = &ctx.data.party[name];
            resolutions[name] = if quest.as_ref() == Some(&Quest::Park) {
                let fatigue_impact = match ctx.data.building_levels[Building::Park].current {
                    1 => 30,
                    2 => 50,
                    _ => 70,
                };
                Resolution {
                    quest: Quest::Park,
                    success: true,
                    reward: 0,
                    experience: 0,
                    fatigue_impact,
                    chance: 100,
                }
            } else if quest.as_ref() == Some(&Quest::Library) {
                let experience = match ctx.data.building_levels[Building::Library].current {
                    1 => 75,
                    2 => 125,
                    _ => 175,
                };
                Resolution {
                    quest: Quest::Library,
                    success: true,
                    reward: 0,
                    experience,
                    fatigue_impact: 0,
                    chance: 100,
                }
            } else if quest.as_ref() == Some(&Quest::Mine) {
                let reward = match ctx.data.building_levels[Building::Mine].current {
                    1 => 400,
                    2 => 600,
                    _ => 700,
                };
                Resolution {
                    quest: Quest::Mine,
                    success: true,
                    reward,
                    experience: 0,
                    fatigue_impact: -15,
                    chance: 100,
                }
            } else if let Some(dungeon_name) = quest {
                let dungeon = &ctx.data.dungeons[*dungeon_name];
                Resolution::new(adventurer, dungeon, *dungeon_name)
            } else {
                Resolution::default()
            }
        }

        resolutions
    }

    fn next_character(&mut self) {
        self.current_character = match self.current_character {
            PartyMember::Redd => PartyMember::Violet,
            PartyMember::Violet => PartyMember::Gene,
            PartyMember::Gene => PartyMember::Yelli,
            PartyMember::Yelli => {
                self.exit = true;
                PartyMember::Yelli
            }
        }
    }

    fn apply_resolutions(&self, ctx: &mut Context) {
        for (name, resolution) in self.resolutions.iter() {
            ctx.data.gold += resolution.reward;
            let character = &mut ctx.data.party[name];
            character.fatigue = if resolution.fatigue_impact < 0 {
                character
                    .fatigue
                    .saturating_sub(resolution.fatigue_impact.unsigned_abs())
            } else {
                std::cmp::min(character.fatigue + resolution.fatigue_impact as usize, 100)
            };
            character.experience += resolution.experience;
            let dungeon = &mut ctx.data.dungeons[resolution.quest];
            if resolution.success {
                dungeon.clear_count += 1;
            }
            dungeon.exploration_count += 1;
            character.remove_consumables();
        }
    }
}

#[derive(Debug, Default)]
struct Resolution {
    quest: Quest,
    success: bool,
    reward: usize,
    experience: usize,
    fatigue_impact: isize,
    chance: isize,
}

impl Resolution {
    pub fn new(adventurer: &Adventurer, dungeon: &Dungeon, quest: Quest) -> Self {
        let mut chance = 50;
        let level_diff = adventurer.level() as isize - dungeon.level as isize;
        chance += level_diff * 10;
        chance += adventurer.weapon_impact as isize;

        chance += if adventurer.fatigue < 10 {
            -30
        } else if adventurer.fatigue < 40 {
            -10
        } else if adventurer.fatigue < 80 {
            0
        } else {
            20
        };

        for skill in adventurer.skills.iter() {
            chance += skill.strict_increase();
        }
        for attribute in dungeon.attributes.iter() {
            chance += attribute.effect(&adventurer.skills);
        }

        chance += dungeon.exploration_count as isize * 5;

        let mut success = if chance <= 0 {
            false
        } else {
            fastrand::isize(0..100) < chance
        };

        if !success && adventurer.skills.contains(&Skill::Revive) {
            success = fastrand::isize(0..100) < chance
        };

        let reward = if success {
            dungeon.reward / (dungeon.clear_count + 1)
        } else {
            0
        };

        let fatigue_impact = if success {
            -5
        } else if chance < 10 {
            -20
        } else {
            -10
        };

        let experience = if success { 50 - level_diff * 10 } else { 25 };

        let experience = if experience < 0 { 0 } else { experience as usize };

        Self {
            quest,
            success,
            reward,
            experience,
            fatigue_impact,
            chance,
        }
    }
}
