use idk::color::hex_rgb;
use macroquad::{
    prelude::{Color, Vec2},
    shapes::draw_rectangle,
};

use crate::{building_menu::Building, grid::Grid, scenes::town::TILE_SIZE};

#[derive(Debug, Clone)]
pub struct Polymino {
    pub shape: Vec<(isize, isize)>,
    pub kind: Building,
    pub color: Color,
}

impl Polymino {
    pub fn rotate_cw(&mut self) {
        for (col, row) in self.shape.iter_mut() {
            *row = -(*row);
            std::mem::swap(col, row);
        }
    }

    pub fn rotate_ccw(&mut self) {
        for (col, row) in self.shape.iter_mut() {
            *col = -(*col);
            std::mem::swap(col, row);
        }
    }

    pub fn mirror(&mut self) {
        for (col, _) in self.shape.iter_mut() {
            *col = -(*col);
        }
    }

    pub fn draw(&self, position: Vec2) {
        for (col, row) in &self.shape {
            draw_rectangle(
                position.x + TILE_SIZE * *col as f32 - TILE_SIZE / 2.0,
                position.y + TILE_SIZE * *row as f32 - TILE_SIZE / 2.0,
                TILE_SIZE,
                TILE_SIZE,
                self.color,
            );
        }
    }

    pub fn draw_ghost(&self, position: Vec2) {
        let mut color = self.color;
        color.a = 0.7;
        for (col, row) in &self.shape {
            draw_rectangle(
                position.x + TILE_SIZE * *col as f32,
                position.y + TILE_SIZE * *row as f32,
                TILE_SIZE,
                TILE_SIZE,
                color,
            );
        }
    }

    pub fn draw_placed(&self, col: usize, row: usize, grid: &Grid) {
        let position = grid.grid_index_to_screen_coords(col, row);
        for (col, row) in &self.shape {
            draw_rectangle(
                position.x + TILE_SIZE * *col as f32,
                position.y + TILE_SIZE * *row as f32,
                TILE_SIZE,
                TILE_SIZE,
                self.color,
            );
        }
    }

    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------

    pub fn inn() -> Self {
        let shape = vec![(-1, -1), (-1, 0), (0, 0), (1, 0), (1, -1)];
        Self {
            shape,
            kind: Building::Inn,
            color: hex_rgb(0xFFCC00),
        }
    }

    pub fn blacksmith() -> Self {
        let shape = vec![(-1, -1), (-1, 0), (0, 0), (1, 0)];
        Self {
            shape,
            kind: Building::Blacksmith,
            color: hex_rgb(0xAA3333),
        }
    }

    pub fn academy() -> Self {
        let mut p = Self::blacksmith();
        p.kind = Building::Academy;
        p.color = hex_rgb(0x5588CC);
        p.mirror();
        p
    }

    pub fn park() -> Self {
        let shape = vec![(-1, -1), (-1, 0), (0, 0), (1, 0), (0, 1)];
        Self {
            shape,
            kind: Building::Park,
            color: hex_rgb(0x44BB44),
        }
    }

    pub fn bakery() -> Self {
        let shape = vec![(0, 0), (1, 0), (0, 1)];
        Self {
            shape,
            kind: Building::Bakery,
            color: hex_rgb(0xDD9999),
        }
    }

    pub fn potion_shop() -> Self {
        let shape = vec![(-1, 1), (0, 1), (0, 0), (1, 0), (1, -1)];
        Self {
            shape,
            kind: Building::PotionShop,
            color: hex_rgb(0xAA22BB),
        }
    }

    pub fn library() -> Self {
        let shape = vec![(-1, 0), (0, 0), (0, -1), (1, 0)];
        Self {
            shape,
            kind: Building::Library,
            color: hex_rgb(0xAABB44),
        }
    }

    pub fn mine() -> Self {
        let shape = vec![(-1, 0), (0, 0), (0, -1), (1, -1)];
        Self {
            shape,
            kind: Building::Mine,
            color: hex_rgb(0xDDDDDD),
        }
    }
}
