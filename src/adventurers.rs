use enum_map::{enum_map, EnumMap};
use macroquad::prelude::*;

use crate::{
    assets::{Gfx, Ttf},
    scenes::quest::PartyMember,
    Context,
};

pub const DESC_BOX_TOP_LEFT: Vec2 = vec2(45.0, 530.0);

pub struct Adventurer {
    pub experience: usize,
    pub weapon_impact: usize,
    pub fatigue: usize,
    pub skills: Vec<Skill>,
}

impl Adventurer {
    pub fn level(&self) -> usize {
        self.experience / 100 + 1
    }

    pub fn add_skill(&mut self, skill: Skill) {
        if !self.skills.contains(&skill) {
            self.skills.push(skill);
        }
    }

    pub fn remove_consumables(&mut self) {
        let consumables = [Skill::Bread, Skill::HealthPotion, Skill::Cookies];
        self.skills.retain(|i| !consumables.contains(i));
    }
}

pub fn draw_adventurer(ctx: &Context, name: PartyMember, x: f32, y: f32) {
    if ctx.data.party[name].fatigue > 30 {
        match name {
            PartyMember::Redd => draw_texture(ctx.assets.gfx(Gfx::Redd), x, y, WHITE),
            PartyMember::Violet => draw_texture(ctx.assets.gfx(Gfx::Violet), x, y, WHITE),
            PartyMember::Gene => draw_texture(ctx.assets.gfx(Gfx::Gene), x, y, WHITE),
            PartyMember::Yelli => draw_texture(ctx.assets.gfx(Gfx::Yelli), x, y, WHITE),
        }
    } else {
        match name {
            PartyMember::Redd => draw_texture(ctx.assets.gfx(Gfx::ReddTired), x, y, WHITE),
            PartyMember::Violet => draw_texture(ctx.assets.gfx(Gfx::VioletTired), x, y, WHITE),
            PartyMember::Gene => draw_texture(ctx.assets.gfx(Gfx::GeneTired), x, y, WHITE),
            PartyMember::Yelli => draw_texture(ctx.assets.gfx(Gfx::YelliTired), x, y, WHITE),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Skill {
    StrengthBoost,
    SpeedBoost,
    MagicBoost,
    DefenseBoost,
    FireResistance,
    IceResistance,
    ElectricResistance,
    PoisonResistance,
    Bread,
    HealthPotion,
    Cookies,
    FireAttack,
    IceAttack,
    ElectricAttack,
    Barrier,
    Revive,
    Armor,
}

impl Skill {
    pub fn strict_increase(&self) -> isize {
        match self {
            Skill::Bread => 5,
            Skill::HealthPotion => 10,
            Skill::Cookies => 2,
            Skill::Barrier => 20,
            Skill::Armor => 15,
            _ => 0,
        }
    }
}

pub fn load_party() -> EnumMap<PartyMember, Adventurer> {
    enum_map! {
        PartyMember::Redd => Adventurer {
            experience: 0,
            weapon_impact: 5,
            fatigue: 80,
            skills: vec![
                Skill::StrengthBoost,
            ]
        },
        PartyMember::Violet => Adventurer {
            experience: 0,
            weapon_impact: 3,
            fatigue: 80,
            skills: vec![
                Skill::SpeedBoost,
            ]
        },
        PartyMember::Gene => Adventurer {
            experience: 0,
            weapon_impact: 2,
            fatigue: 80,
            skills: vec![
                Skill::MagicBoost,
                Skill::ElectricAttack,
            ]
        },
        PartyMember::Yelli => Adventurer {
            experience: 0,
            weapon_impact: 3,
            fatigue: 80,
            skills: vec![
                Skill::DefenseBoost,
            ]
        },
    }
}

pub fn draw_character_desc(ctx: &Context, name: PartyMember) {
    let params = TextParams {
        font: ctx.assets.fonts(Ttf::Munro),
        font_size: 20,
        color: WHITE,
        ..Default::default()
    };

    let character = &ctx.data.party[name];

    match name {
        PartyMember::Redd => draw_text_ex(
            &format!("Redd - Warrior - Level {}", character.level()),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y,
            params.clone(),
        ),
        PartyMember::Violet => draw_text_ex(
            &format!("Violet - \"Treasure Hunter\" - Level {}", character.level()),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y,
            params.clone(),
        ),
        PartyMember::Gene => draw_text_ex(
            &format!("Gene - Mage - Level {}", character.level()),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y,
            params.clone(),
        ),
        PartyMember::Yelli => draw_text_ex(
            &format!("Yelli - Paladin - Level {}", character.level()),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y,
            params.clone(),
        ),
    }

    draw_text_ex(
        "Skills:",
        DESC_BOX_TOP_LEFT.x,
        DESC_BOX_TOP_LEFT.y + 25.0,
        params.clone(),
    );
    draw_text_ex(
        &format!("{:?}", character.skills.iter().take(7).collect::<Vec<&Skill>>()),
        DESC_BOX_TOP_LEFT.x,
        DESC_BOX_TOP_LEFT.y + 50.0,
        params.clone(),
    );
    if character.skills.len() > 5 {
        draw_text_ex(
            &format!("{:?}", character.skills.iter().skip(7).collect::<Vec<&Skill>>()),
            DESC_BOX_TOP_LEFT.x,
            DESC_BOX_TOP_LEFT.y + 75.0,
            params.clone(),
        );
    }
}
