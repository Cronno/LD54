use enum_map::{Enum, EnumMap};
use idk::{
    menu::menu_tile::MenuTile,
    rect::{draw_rect, draw_rect_lines},
};
use macroquad::{
    prelude::{Vec2, BLACK, GRAY, WHITE},
    text::{draw_text_ex, TextParams},
};
use serde::{Deserialize, Serialize};

use crate::{assets::Ttf, descriptions::draw_building_description, scenes::town::PlacedBuilding, Context};

#[derive(Debug, Clone, Copy, Enum, PartialEq, Eq, Serialize, Deserialize, strum::Display, Hash)]
pub enum Building {
    Inn,
    Blacksmith,
    Academy,
    Bakery,
    PotionShop,
    Park,
    Library,
    Mine,
}

pub struct BuildingMenu {
    tiles: EnumMap<Building, MenuTile<Building>>,
    available: Vec<Building>,
}

impl BuildingMenu {
    pub fn new() -> Self {
        let tiles: EnumMap<Building, MenuTile<Building>> =
            ron::de::from_bytes(include_bytes!(r"../menus/building_menu.ron")).unwrap();

        Self {
            tiles,
            available: Vec::new(),
        }
    }

    pub fn update(&mut self, placed_buildings: &EnumMap<Building, Option<PlacedBuilding>>) {
        self.available = placed_buildings
            .iter()
            .filter(|(_, building)| building.is_none())
            .map(|(name, _)| name)
            .collect();
    }

    pub fn click(&self, position: Vec2) -> Option<Building> {
        for (name, tile) in self.tiles.iter() {
            if self.available.contains(&name) && tile.area.contains(position) {
                return Some(name);
            }
        }
        None
    }

    pub fn draw(&self, ctx: &Context, mouse_pos: Vec2) {
        let mut params = TextParams {
            font: ctx.assets.fonts(Ttf::Munro),
            font_size: 32,
            color: WHITE,
            ..Default::default()
        };
        for (name, tile) in self.tiles.iter() {
            draw_rect(&tile.area, BLACK);
            params.color = if self.available.contains(&name) { WHITE } else { GRAY };
            draw_text_ex(
                &name.to_string(),
                tile.area.x + 40.0,
                tile.area.y + 40.0,
                params.clone(),
            );
            if tile.area.contains(mouse_pos) {
                draw_rect_lines(&tile.area, 5.0, params.color);
                draw_building_description(ctx, name);
            }
        }
    }
}

pub fn building_cost(building: Building) -> usize {
    match building {
        Building::Inn => 0,
        Building::Blacksmith => 400,
        Building::Academy => 500,
        Building::Bakery => 100,
        Building::PotionShop => 800,
        Building::Park => 200,
        Building::Library => 300,
        Building::Mine => 100,
    }
}
