mod adventurers;
mod assets;
mod building_menu;
mod descriptions;
mod dungeons;
mod grid;
mod polymino;
mod scenes;
mod settings;

use std::time::Duration;

use adventurers::{load_party, Adventurer};
use assets::{Assets, Audio};
use building_menu::Building;
use dungeons::{load_dungeons, Dungeon};
use enum_map::{Enum, EnumMap};
use idk::{input::InputCollector, scene::Director};
use macroquad::prelude::*;
use scenes::{
    quest::{PartyMember, Quest},
    title::TitleScene,
    town::TownScene,
    upgrade::{load_building_levels, BuildingLevel},
};
use serde::{Deserialize, Serialize};
use settings::Settings;

const TIME_STEP: f32 = 1.0 / 60.0;
const SCREEN_WIDTH: usize = 1280;
const SCREEN_HEIGHT: usize = 720;

pub struct Context {
    assets: Assets,
    audio: Audio,
    data: Data,
    settings: Settings,
}

#[derive(Debug, Serialize, Deserialize, Enum, Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameInput {
    Mirror,
    Exit,
}

fn window_conf() -> Conf {
    Conf {
        window_title: "idk".to_owned(),
        window_width: SCREEN_WIDTH as i32,
        window_height: SCREEN_HEIGHT as i32,
        window_resizable: false,
        icon: None,
        ..Default::default()
    }
}

pub struct Data {
    pub party: EnumMap<PartyMember, Adventurer>,
    pub dungeons: EnumMap<Quest, Dungeon>,
    pub gold: usize,
    pub building_levels: EnumMap<Building, BuildingLevel>,
    pub day: usize,
}

impl Data {
    pub async fn load() -> Self {
        Self {
            party: load_party(),
            dungeons: load_dungeons(),
            gold: 1000,
            building_levels: load_building_levels(),
            day: 1,
        }
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let env = env_logger::Env::default().filter_or("RUST_LOG", env!("CARGO_PKG_NAME"));
    env_logger::init_from_env(env);

    let mut ctx = Context {
        assets: Assets::load().await,
        audio: Audio::load().await,
        data: Data::load().await,
        settings: Settings::load().await,
    };
    let mut input_events = InputCollector::new();
    let delta = Duration::from_secs_f32(TIME_STEP);

    let mut director = Director::new(TitleScene::new(&ctx));
    let mut elapsed = 0.0;
    loop {
        if is_key_pressed(KeyCode::Home) {
            ctx.settings.show_debug = !ctx.settings.show_debug;
        }

        if is_key_pressed(KeyCode::KpAdd) {
            ctx.data.day += 1;
        }

        elapsed += get_frame_time();
        input_events.collect_events();
        while elapsed >= TIME_STEP {
            director.update(&mut ctx, &input_events, delta);
            input_events.clear_events();
            elapsed -= TIME_STEP;
        }
        director.render(&ctx);

        next_frame().await
    }
}
