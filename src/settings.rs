use std::collections::HashMap;

use idk::{assets::load_data, input::InputType, menu::menu_scene::MenuInput};

use crate::GameInput;

pub struct Settings {
    pub game_input_config: HashMap<GameInput, Vec<InputType>>,
    pub menu_input_config: HashMap<MenuInput, Vec<InputType>>,
    pub show_hitboxes: bool,
    pub show_debug: bool,
}

impl Settings {
    pub async fn load() -> Self {
        let game_input_config = match load_data("config/control_config.ron").await {
            Ok(config) => config,
            Err(e) => {
                log::error!("{e}");
                Self::default().game_input_config
            }
        };

        let menu_input_config = match load_data("config/menu_control_config.ron").await {
            Ok(config) => config,
            Err(e) => {
                log::error!("{e}");
                Self::default().menu_input_config
            }
        };

        Self {
            game_input_config,
            menu_input_config,
            show_hitboxes: true,
            show_debug: false,
        }
    }
}

impl Default for Settings {
    fn default() -> Self {
        let game_input_config = ron::de::from_bytes(include_bytes!("../config/control_config.ron")).unwrap();
        let menu_input_config = ron::de::from_bytes(include_bytes!("../config/menu_control_config.ron")).unwrap();

        Self {
            game_input_config,
            menu_input_config,
            show_hitboxes: true,
            show_debug: true,
        }
    }
}
