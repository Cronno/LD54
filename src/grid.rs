use idk::color::hex_rgb;
use macroquad::prelude::*;

use crate::{
    polymino::Polymino,
    scenes::town::{PlacedBuilding, TownElement, DESTROY_TOP_LEFT, TILE_SIZE, TOWN_TOP_LEFT},
};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum GridKind {
    Town,
    Destroy,
}

#[derive(Debug, Clone)]
pub struct Grid {
    pub top_left: Vec2,
    pub grid: Vec<Vec<TownElement>>,
    pub ghost_position: Option<(usize, usize)>,
    pub kind: GridKind,
    pub area: Rect,
}

impl Grid {
    pub fn new_town(matrix: Vec<Vec<TownElement>>) -> Self {
        let area = Rect::new(
            TOWN_TOP_LEFT.x,
            TOWN_TOP_LEFT.y,
            matrix.len() as f32 * TILE_SIZE,
            matrix[0].len() as f32 * TILE_SIZE,
        );
        Self {
            top_left: TOWN_TOP_LEFT,
            grid: matrix,
            ghost_position: None,
            kind: GridKind::Town,
            area,
        }
    }

    pub fn new_destroy(matrix: Vec<Vec<TownElement>>) -> Self {
        let area = Rect::new(
            DESTROY_TOP_LEFT.x,
            DESTROY_TOP_LEFT.y,
            matrix.len() as f32 * TILE_SIZE,
            matrix[0].len() as f32 * TILE_SIZE,
        );
        Self {
            top_left: DESTROY_TOP_LEFT,
            grid: matrix,
            ghost_position: None,
            kind: GridKind::Destroy,
            area,
        }
    }

    pub fn grid_index_at_point(&self, point: Vec2) -> Option<(usize, usize)> {
        for (col_i, col) in self.grid.iter().enumerate() {
            for (row_i, _) in col.iter().enumerate() {
                let rect = Rect::new(
                    self.top_left.x + TILE_SIZE * col_i as f32,
                    self.top_left.y + TILE_SIZE * row_i as f32,
                    TILE_SIZE,
                    TILE_SIZE,
                );
                if rect.contains(point) {
                    return Some((col_i, row_i));
                }
            }
        }
        None
    }

    pub fn grid_index_to_screen_coords(&self, col: usize, row: usize) -> Vec2 {
        vec2(
            self.top_left.x + TILE_SIZE * col as f32,
            self.top_left.y + TILE_SIZE * row as f32,
        )
    }

    pub fn index_is_empty(&self, col: usize, row: usize) -> bool {
        if let Some(element) = self.grid.get(col).and_then(|c| c.get(row)) {
            *element == TownElement::Empty
        } else {
            false
        }
    }

    pub fn update_ghost_piece(&mut self, mouse_pos: Vec2) {
        self.ghost_position = if self.hovered(mouse_pos) {
            self.grid_index_at_point(mouse_pos)
        } else {
            None
        }
    }

    pub fn set_building(&mut self, mino: &Polymino) -> Option<PlacedBuilding> {
        let Some(position) = self.ghost_position else {
            return None;
        };

        for offset in mino.shape.iter() {
            let col = (position.0 as isize + offset.0) as usize;
            let row = (position.1 as isize + offset.1) as usize;
            if !self.index_is_empty(col, row) {
                return None;
            }
        }

        for offset in mino.shape.iter() {
            let col = (position.0 as isize + offset.0) as usize;
            let row = (position.1 as isize + offset.1) as usize;
            if let Some(element) = self.grid.get_mut(col).and_then(|c| c.get_mut(row)) {
                *element = TownElement::Building;
            }
        }

        self.ghost_position = None;

        Some(PlacedBuilding {
            mino: mino.clone(),
            col: position.0,
            row: position.1,
            grid: self.kind,
        })
    }

    pub fn hovered(&self, mouse_pos: Vec2) -> bool {
        self.area.contains(mouse_pos)
    }

    pub fn remove(&mut self, building: &PlacedBuilding) {
        for (col, row) in building.indices() {
            self.grid[col][row] = TownElement::Empty;
        }
    }

    pub fn draw(&self) {
        for (col_i, col) in self.grid.iter().enumerate() {
            for (row_i, element) in col.iter().enumerate() {
                let color = match element {
                    TownElement::Empty => hex_rgb(0x7D8CA1),
                    TownElement::Wall => hex_rgb(0x26263D),
                    TownElement::Building => RED,
                    TownElement::Outside => continue,
                };
                if self.kind == GridKind::Town {
                    draw_rectangle(
                        self.top_left.x + TILE_SIZE * col_i as f32,
                        self.top_left.y + TILE_SIZE * row_i as f32,
                        TILE_SIZE,
                        TILE_SIZE,
                        color,
                    );
                }
                draw_rectangle_lines(
                    self.top_left.x + TILE_SIZE * col_i as f32,
                    self.top_left.y + TILE_SIZE * row_i as f32,
                    TILE_SIZE,
                    TILE_SIZE,
                    0.5,
                    BLACK,
                );
            }
        }
    }
}
