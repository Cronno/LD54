use std::fmt::Display;

use enum_map::{Enum, EnumArray, EnumMap};
use macroquad::prelude::*;
use serde::de::DeserializeOwned;

use crate::animation::SpriteData;
pub trait AssetType: Clone + Copy + Enum + Display + strum::IntoEnumIterator {}

#[derive(Debug)]
pub struct Assets<Gfx, Sheet, Ttf>
where
    Gfx: AssetType + EnumArray<Option<Texture2D>>,
    Sheet: AssetType + EnumArray<Option<Texture2D>> + EnumArray<Option<SpriteData>>,
    Ttf: AssetType + EnumArray<Option<Font>>,
{
    gfx: EnumMap<Gfx, Option<Texture2D>>,
    not_found: Texture2D,

    sprite_sheets: EnumMap<Sheet, Option<Texture2D>>,
    sprite_data: EnumMap<Sheet, Option<SpriteData>>,

    fonts: EnumMap<Ttf, Option<Font>>,
}

impl<Gfx, Sheet, Ttf> Assets<Gfx, Sheet, Ttf>
where
    Gfx: AssetType + EnumArray<Option<Texture2D>>,
    Sheet: AssetType + EnumArray<Option<Texture2D>> + EnumArray<Option<SpriteData>>,
    Ttf: AssetType + EnumArray<Option<Font>>,
{
    pub async fn load() -> Self {
        let not_found = Texture2D::from_file_with_format(include_bytes!("../assets/gfx/not_found.png"), None);
        let mut gfx = EnumMap::default();
        for item in Gfx::iter() {
            gfx[item] = match load_texture(&format!("assets/gfx/{}.png", item)).await {
                Ok(gfx) => Some(gfx),
                Err(e) => {
                    log::error!("Failed to load: assets/gfx/{}.png - {}", item, e);
                    None
                }
            }
        }

        let mut sprite_sheets = EnumMap::default();
        let mut sprite_data = EnumMap::default();
        for item in Sheet::iter() {
            sprite_sheets[item] = match load_texture(&format!("assets/gfx/{}.png", item)).await {
                Ok(gfx) => Some(gfx),
                Err(e) => {
                    log::error!("Failed to load: assets/gfx/{}.png - {}", item, e);
                    None
                }
            };
            sprite_data[item] = match load_data(&format!("assets/data/{}.ron", item)).await {
                Ok(data) => Some(data),
                Err(e) => {
                    log::error!("Failed to load: assets/data/{}.ron - {}", item, e);
                    None
                }
            }
        }

        let mut fonts = EnumMap::default();
        for item in Ttf::iter() {
            fonts[item] = match load_ttf_font(&format!("assets/fonts/{}.ttf", item)).await {
                Ok(font) => Some(font),
                Err(e) => {
                    log::error!("Failed to load: assets/fonts/{}.ttf - {}", item, e);
                    None
                }
            }
        }

        Self {
            gfx,
            not_found,
            sprite_sheets,
            sprite_data,
            fonts,
        }
    }

    pub fn gfx(&self, item: Gfx) -> &Texture2D {
        match &self.gfx[item] {
            Some(texture) => texture,
            None => &self.not_found,
        }
    }

    pub fn sprite_sheets(&self, item: Sheet) -> &Texture2D {
        match &self.sprite_sheets[item] {
            Some(sheet) => sheet,
            None => &self.not_found,
        }
    }

    pub fn sprite_data(&self, item: Sheet) -> &SpriteData {
        match &self.sprite_data[item] {
            Some(data) => data,
            None => panic!(),
        }
    }

    pub fn fonts(&self, item: Ttf) -> Option<&Font> {
        match &self.fonts[item] {
            Some(font) => Some(font),
            None => panic!(),
        }
    }
}

pub struct SpriteSheets {}

pub async fn load_data<T: DeserializeOwned>(file_path: &str) -> Result<T, String> {
    let data = macroquad::file::load_file(file_path)
        .await
        .map_err(|e| format!("Error opening {}: {}", file_path, e))?;
    ron::de::from_bytes(&data).map_err(|e| format!("Error reading {}: {}", file_path, e))
}
