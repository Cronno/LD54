use std::collections::{HashMap, HashSet};

use enum_map::{Enum, EnumArray, EnumMap};
use gilrs::{Axis, Button, GamepadId, Gilrs};
use macroquad::{
    miniquad::{EventHandler, KeyMods},
    prelude::{mouse_position, utils, KeyCode, MouseButton, Vec2},
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq, Hash)]
pub enum InputType {
    #[serde(with = "KeyCodeDef")]
    Key(KeyCode),
    Btn(Button),
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Enum)]
pub enum MouseInput {
    Left,
    Middle,
    Right,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MouseWheel {
    Up,
    Neutral,
    Down,
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub enum InputState {
    #[default]
    Up,
    Pressed,
    Down,
    Released,
}

#[derive(Debug, Clone, Copy)]
pub enum InputEvent {
    Pressed,
    Released,
}

pub struct InputHandler<T: EnumArray<InputState> + EnumArray<u8> + Copy> {
    states: EnumMap<T, InputState>,
    mouse_states: EnumMap<MouseInput, InputState>,
    bindings: HashMap<InputType, Vec<T>>,
    pressed_counter: EnumMap<T, u8>,
    down_map: HashSet<InputType>,
    previous_mouse_pos: Vec2,
    mouse_pos: Vec2,
    mouse_wheel: MouseWheel,

    _active_gamepad: Option<GamepadId>,
    left_stick: Vec2,
    right_stick: Vec2,
}

impl<T: EnumArray<InputState> + EnumArray<u8> + Copy> InputHandler<T> {
    pub fn new(mapping: &HashMap<T, Vec<InputType>>) -> Self {
        let mut bindings: HashMap<InputType, Vec<T>> = HashMap::new();
        for (action, buttons) in mapping {
            for button in buttons {
                bindings.entry(*button).or_default().push(*action);
            }
        }

        Self {
            states: EnumMap::default(),
            mouse_states: EnumMap::default(),
            bindings,
            pressed_counter: EnumMap::default(),
            down_map: HashSet::new(),
            previous_mouse_pos: mouse_position().into(),
            mouse_pos: mouse_position().into(),
            mouse_wheel: MouseWheel::Neutral,
            _active_gamepad: None,
            left_stick: Vec2::default(),
            right_stick: Vec2::default(),
        }
    }

    pub fn update(&mut self, events: &InputCollector) {
        self.advance_states();

        for (button, state) in &events.input_events {
            self.handle_input_event(*button, *state);
        }
        for (button, state) in &events.mouse_events {
            self.handle_mouse_input_event(*button, *state);
        }
        for (axis, value) in &events.analog_events {
            self.handle_analog_event(*axis, *value);
        }
        for direction in &events.mouse_wheel_events {
            self.handle_mouse_wheel_event(*direction);
        }
    }

    pub fn handle_input_event(&mut self, button: InputType, state: InputEvent) {
        let Some(actions) = self.bindings.get(&button) else {
            return;
        };
        for action in actions {
            match state {
                InputEvent::Pressed => {
                    if !self.down_map.contains(&button) {
                        self.down_map.insert(button);
                        if self.pressed_counter[*action] == 0 {
                            self.states[*action] = InputState::Pressed;
                        }
                        self.pressed_counter[*action] = self.pressed_counter[*action].saturating_add(1);
                    }
                }
                InputEvent::Released => {
                    self.down_map.remove(&button);
                    self.pressed_counter[*action] = self.pressed_counter[*action].saturating_sub(1);
                    if self.pressed_counter[*action] == 0 {
                        self.states[*action] = InputState::Released;
                    }
                }
            }
        }
    }

    pub fn handle_mouse_input_event(&mut self, button: MouseButton, state: InputEvent) {
        let input = match button {
            MouseButton::Left => MouseInput::Left,
            MouseButton::Middle => MouseInput::Middle,
            MouseButton::Right => MouseInput::Right,
            MouseButton::Unknown => MouseInput::Middle,
        };

        match state {
            InputEvent::Pressed => self.mouse_states[input] = InputState::Pressed,
            InputEvent::Released => self.mouse_states[input] = InputState::Released,
        }
    }

    pub fn handle_analog_event(&mut self, axis: Axis, value: f32) {
        match axis {
            Axis::LeftStickX => self.left_stick.x = value,
            Axis::LeftStickY => self.left_stick.y = value,
            Axis::RightStickX => self.right_stick.x = value,
            Axis::RightStickY => self.right_stick.y = value,
            _ => {}
        }
    }

    pub fn handle_mouse_wheel_event(&mut self, direction: MouseWheel) {
        self.mouse_wheel = direction;
    }

    pub fn advance_states(&mut self) {
        for (_, state) in self.states.iter_mut() {
            if *state == InputState::Pressed {
                *state = InputState::Down;
            } else if *state == InputState::Released {
                *state = InputState::Up;
            }
        }

        self.previous_mouse_pos = self.mouse_pos;
        self.mouse_pos = mouse_position().into();
        for (_, state) in self.mouse_states.iter_mut() {
            if *state == InputState::Pressed {
                *state = InputState::Down;
            } else if *state == InputState::Released {
                *state = InputState::Up;
            }
        }
        self.mouse_wheel = MouseWheel::Neutral;
    }

    pub fn clear(&mut self) {
        self.down_map.clear();
        self.states.clear();
        self.mouse_states.clear();
        self.pressed_counter.clear();
    }

    pub fn pressed(&self, input: T) -> bool {
        self.states[input] == InputState::Pressed
    }

    pub fn down(&self, input: T) -> bool {
        self.states[input] == InputState::Down || self.states[input] == InputState::Pressed
    }

    pub fn released(&self, input: T) -> bool {
        self.states[input] == InputState::Released
    }

    pub fn mouse_pressed(&self, input: MouseInput) -> bool {
        self.mouse_states[input] == InputState::Pressed
    }

    pub fn mouse_down(&self, input: MouseInput) -> bool {
        self.mouse_states[input] == InputState::Down || self.mouse_states[input] == InputState::Pressed
    }

    pub fn mouse_released(&self, input: MouseInput) -> bool {
        self.mouse_states[input] == InputState::Released
    }

    pub fn mouse_pos(&self) -> Vec2 {
        self.mouse_pos
    }

    pub fn mouse_delta(&self) -> Vec2 {
        self.mouse_pos - self.previous_mouse_pos
    }

    pub fn left_stick_position(&self) -> Vec2 {
        self.left_stick
    }

    pub fn right_stick_position(&self) -> Vec2 {
        self.right_stick
    }

    pub fn mouse_wheel_rolled(&self, direction: MouseWheel) -> bool {
        self.mouse_wheel == direction
    }
}

#[derive(Debug)]
pub struct InputCollector {
    gilrs: Gilrs,
    miniquad_sub_id: usize,
    input_events: Vec<(InputType, InputEvent)>,
    mouse_events: Vec<(MouseButton, InputEvent)>,
    analog_events: Vec<(Axis, f32)>,
    mouse_wheel_events: Vec<MouseWheel>,
}

impl InputCollector {
    pub fn new() -> Self {
        Self {
            gilrs: Gilrs::new().unwrap(),
            miniquad_sub_id: utils::register_input_subscriber(),
            input_events: Vec::new(),
            mouse_events: Vec::new(),
            analog_events: Vec::new(),
            mouse_wheel_events: Vec::new(),
        }
    }

    pub fn collect_events(&mut self) {
        utils::repeat_all_miniquad_input(self, self.miniquad_sub_id);
        self.get_gilrs_events();
    }

    pub fn clear_events(&mut self) {
        self.input_events.clear();
        self.mouse_events.clear();
        self.analog_events.clear();
        self.mouse_wheel_events.clear();
    }

    pub fn get_gilrs_events(&mut self) {
        while let Some(event) = self.gilrs.next_event() {
            match event.event {
                gilrs::EventType::ButtonPressed(button, _) => {
                    self.input_events.push((InputType::Btn(button), InputEvent::Pressed))
                }
                gilrs::EventType::ButtonReleased(button, _) => {
                    self.input_events.push((InputType::Btn(button), InputEvent::Released))
                }
                gilrs::EventType::AxisChanged(axis, value, _) => self.analog_events.push((axis, value)),
                _ => {}
            }
        }
    }
}

impl Default for InputCollector {
    fn default() -> Self {
        Self::new()
    }
}

impl EventHandler for InputCollector {
    fn update(&mut self) {}
    fn draw(&mut self) {}
    fn key_down_event(&mut self, keycode: KeyCode, _keymods: KeyMods, repeat: bool) {
        if !repeat {
            self.input_events.push((InputType::Key(keycode), InputEvent::Pressed));
        }
    }

    fn key_up_event(&mut self, keycode: KeyCode, _keymods: KeyMods) {
        self.input_events.push((InputType::Key(keycode), InputEvent::Released));
    }

    fn mouse_button_down_event(&mut self, button: MouseButton, _x: f32, _y: f32) {
        self.mouse_events.push((button, InputEvent::Pressed));
    }

    fn mouse_button_up_event(&mut self, button: MouseButton, _x: f32, _y: f32) {
        self.mouse_events.push((button, InputEvent::Released));
    }

    fn mouse_wheel_event(&mut self, _x: f32, y: f32) {
        if y > 0.0 {
            self.mouse_wheel_events.push(MouseWheel::Up);
        } else if y < 0.0 {
            self.mouse_wheel_events.push(MouseWheel::Down);
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "KeyCode")]
pub enum KeyCodeDef {
    Space,
    Apostrophe,
    Comma,
    Minus,
    Period,
    Slash,
    Key0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    Semicolon,
    Equal,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    LeftBracket,
    Backslash,
    RightBracket,
    GraveAccent,
    World1,
    World2,
    Escape,
    Enter,
    Tab,
    Backspace,
    Insert,
    Delete,
    Right,
    Left,
    Down,
    Up,
    PageUp,
    PageDown,
    Home,
    End,
    CapsLock,
    ScrollLock,
    NumLock,
    PrintScreen,
    Pause,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    F13,
    F14,
    F15,
    F16,
    F17,
    F18,
    F19,
    F20,
    F21,
    F22,
    F23,
    F24,
    F25,
    Kp0,
    Kp1,
    Kp2,
    Kp3,
    Kp4,
    Kp5,
    Kp6,
    Kp7,
    Kp8,
    Kp9,
    KpDecimal,
    KpDivide,
    KpMultiply,
    KpSubtract,
    KpAdd,
    KpEnter,
    KpEqual,
    LeftShift,
    LeftControl,
    LeftAlt,
    LeftSuper,
    RightShift,
    RightControl,
    RightAlt,
    RightSuper,
    Menu,
    Unknown,
}
