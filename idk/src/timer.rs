use std::time::Duration;

#[derive(Debug, Clone, Copy)]
pub struct Stopwatch {
    elapsed: Duration,
    paused: bool,
}

impl Stopwatch {
    pub fn new() -> Self {
        Self {
            elapsed: Duration::default(),
            paused: false,
        }
    }

    pub fn tick(&mut self, delta: Duration) {
        if !self.paused {
            self.elapsed += delta;
        }
    }

    pub fn elapsed(&self) -> Duration {
        self.elapsed
    }

    pub fn reset(&mut self) {
        self.elapsed = Duration::default();
    }

    pub fn pause(&mut self) {
        self.paused = true;
    }

    pub fn unpause(&mut self) {
        self.paused = false;
    }

    pub fn is_paused(&self) -> bool {
        self.paused
    }
}

impl Default for Stopwatch {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Timer {
    stopwatch: Stopwatch,
    duration: Duration,
    finished: bool,
}

impl Timer {
    pub fn new(mut duration: Duration) -> Self {
        if duration.is_zero() {
            // If the duration could be 0 then "while timer.finished() {...}" would cause infinite loops
            duration = Duration::from_millis(1);
        }
        Self {
            stopwatch: Stopwatch::new(),
            duration,
            finished: false,
        }
    }

    pub fn tick(&mut self, delta: Duration) {
        self.stopwatch.tick(delta);
        self.finished = self.stopwatch.elapsed >= self.duration;
    }

    pub fn finished(&self) -> bool {
        self.finished
    }

    pub fn remaining(&self) -> Duration {
        self.duration.saturating_sub(self.stopwatch.elapsed)
    }

    pub fn reset(&mut self) {
        self.finished = false;
        self.stopwatch.reset();
    }

    pub fn percent(&self) -> f64 {
        self.stopwatch.elapsed.as_secs_f64() / self.duration.as_secs_f64()
    }

    pub fn pause(&mut self) {
        self.stopwatch.paused = true;
    }

    pub fn unpause(&mut self) {
        self.stopwatch.paused = false;
    }

    pub fn is_paused(&self) -> bool {
        self.stopwatch.paused
    }

    pub fn overflow(&self) -> Duration {
        self.stopwatch.elapsed.saturating_sub(self.duration)
    }
}

pub fn format_time(duration: Duration) -> String {
    let seconds = duration.as_secs_f32();
    let minutes = f32::trunc(seconds / 60.0);
    let seconds = f32::trunc(seconds - 60.0 * minutes);

    format!("{:>02}:{:>02.0}", minutes, seconds)
}
