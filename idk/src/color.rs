use macroquad::{color_u8, prelude::Color};

pub fn hex_rgba(c: u32) -> Color {
    let c = c.to_be_bytes();
    color_u8!(c[0], c[1], c[2], c[3])
}

pub fn hex_rgb(c: u32) -> Color {
    let c = c.to_be_bytes();
    color_u8!(c[1], c[2], c[3], 255)
}
