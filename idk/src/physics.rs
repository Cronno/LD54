use rapier2d::{crossbeam, na::Vector2, prelude::*};

pub struct Physics {
    pub rigid_body_set: RigidBodySet,
    pub collider_set: ColliderSet,
    gravity: Vector2<f32>,
    integration_parameters: IntegrationParameters,
    physics_pipeline: PhysicsPipeline,
    island_manager: IslandManager,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    impulse_joint_set: ImpulseJointSet,
    multibody_joint_set: MultibodyJointSet,
    ccd_solver: CCDSolver,
    event_handler: ChannelEventCollector,
    pub collision_recv: crossbeam::channel::Receiver<CollisionEvent>,
}

impl Physics {
    pub fn new(gravity: Vector2<Real>) -> Self {
        let rigid_body_set = RigidBodySet::new();
        let collider_set = ColliderSet::new();

        let integration_parameters = IntegrationParameters::default();
        let physics_pipeline = PhysicsPipeline::new();
        let island_manager = IslandManager::new();
        let broad_phase = BroadPhase::new();
        let narrow_phase = NarrowPhase::new();
        let impulse_joint_set = ImpulseJointSet::new();
        let multibody_joint_set = MultibodyJointSet::new();
        let ccd_solver = CCDSolver::new();

        let (collision_send, collision_recv) = crossbeam::channel::unbounded();
        let (contact_force_send, _contact_force_recv) = crossbeam::channel::unbounded();
        let event_handler = ChannelEventCollector::new(collision_send, contact_force_send);

        Self {
            rigid_body_set,
            collider_set,
            gravity,
            integration_parameters,
            physics_pipeline,
            island_manager,
            broad_phase,
            narrow_phase,
            impulse_joint_set,
            multibody_joint_set,
            ccd_solver,
            event_handler,
            collision_recv,
        }
    }

    pub fn step(&mut self) {
        self.physics_pipeline.step(
            &self.gravity,
            &self.integration_parameters,
            &mut self.island_manager,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            &mut self.rigid_body_set,
            &mut self.collider_set,
            &mut self.impulse_joint_set,
            &mut self.multibody_joint_set,
            &mut self.ccd_solver,
            None,
            &(),
            &self.event_handler,
        );
    }

    pub fn set_timestep(&mut self, delta: f32) {
        self.integration_parameters.dt = delta;
    }

    pub fn remove_collider(&mut self, handle: ColliderHandle) {
        self.collider_set
            .remove(handle, &mut self.island_manager, &mut self.rigid_body_set, true);
    }

    pub fn insert_collider(&mut self, collider: Collider, parent: Option<RigidBodyHandle>) -> ColliderHandle {
        match parent {
            Some(parent) => self
                .collider_set
                .insert_with_parent(collider, parent, &mut self.rigid_body_set),
            None => self.collider_set.insert(collider),
        }
    }
}
