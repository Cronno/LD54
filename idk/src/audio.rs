use enum_map::{EnumArray, EnumMap};
use macroquad::audio::{load_sound, play_sound, play_sound_once, stop_sound, PlaySoundParams, Sound};

use super::assets::AssetType;

pub struct Audio<Bgm, Sfx>
where
    Bgm: AssetType + EnumArray<Option<Sound>>,
    Sfx: AssetType + EnumArray<Option<Sound>>,
{
    bgm: EnumMap<Bgm, Option<Sound>>,
    sfx: EnumMap<Sfx, Option<Sound>>,
    pub mute: bool,
}

impl<Bgm, Sfx> Audio<Bgm, Sfx>
where
    Bgm: AssetType + EnumArray<Option<Sound>>,
    Sfx: AssetType + EnumArray<Option<Sound>>,
{
    pub async fn load() -> Self {
        let mut bgm = EnumMap::default();
        for item in Bgm::iter() {
            bgm[item] = match load_sound(&format!("assets/bgm/{}.wav", item)).await {
                Ok(bgm) => Some(bgm),
                Err(e) => {
                    log::error!("Failed to load: assets/bgm/{}.wav - {}", item, e);
                    None
                }
            }
        }

        let mut sfx = EnumMap::default();
        for item in Sfx::iter() {
            sfx[item] = match load_sound(&format!("assets/sfx/{}.wav", item)).await {
                Ok(sfx) => Some(sfx),
                Err(e) => {
                    log::error!("Failed to load: assets/sfx/{}.wav - {}", item, e);
                    None
                }
            }
        }
        Self { bgm, sfx, mute: false }
    }

    pub fn play(&self, sfx: Sfx) {
        if !self.mute {
            match &self.sfx[sfx] {
                Some(sound) => play_sound_once(sound),
                None => log::error!("Sound Effect not loaded: {sfx}"),
            };
        }
    }

    pub fn start(&self, bgm: Bgm) {
        if !self.mute {
            match &self.bgm[bgm] {
                Some(sound) => play_sound(
                    sound,
                    PlaySoundParams {
                        looped: true,
                        ..Default::default()
                    },
                ),
                None => log::error!("Music not loaded: {bgm}"),
            };
        }
    }

    pub fn stop(&self, bgm: Bgm) {
        match &self.bgm[bgm] {
            Some(sound) => stop_sound(sound),
            None => log::error!("Music not loaded: {bgm}"),
        };
    }
}
