use enum_map::EnumMap;
use macroquad::prelude::{mouse_position, Rect};
use serde::{Serialize, Deserialize};
use crate::rect::RectDef;

use crate::cursor_control::{HDirection, VDirection};

#[derive(Debug, Serialize, Deserialize)]
pub struct NeighborTiles<T> {
    pub above: Option<T>,
    pub below: Option<T>,
    pub left: Option<T>,
    pub right: Option<T>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MenuTile<T: Copy> {
    #[serde(with = "RectDef")]
    pub area: Rect,
    neighbors: NeighborTiles<T>,
}

impl<T: Copy> MenuTile<T> {
    pub fn new(area: Rect, neighbors: NeighborTiles<T>) -> Self {
        Self {
            area,
            neighbors,
        }
    }

    pub fn vertical_neighbor(&self, dir: VDirection) -> Option<T> {
        match dir {
            VDirection::Up => self.neighbors.above,
            VDirection::Down => self.neighbors.below,
        }
    }

    pub fn horizontal_neighbor(&self, dir: HDirection) -> Option<T> {
        match dir {
            HDirection::Left => self.neighbors.left,
            HDirection::Right => self.neighbors.right,
        }
    }
}

#[derive(Debug)]
pub struct ValueTile {
    value: isize,
    min: isize,
    max: isize,
}

impl ValueTile {
    pub fn new(value: isize, min: isize, max: isize) -> Self {
        Self { value, min, max }
    }

    pub fn modify_value(&mut self, change: isize) {
        self.value += change;
        self.value = self.value.clamp(self.min, self.max);
    }

    pub fn get_value(&self) -> isize {
        self.value
    }
}

pub fn hovered_tile<T>(tiles: &EnumMap<T, MenuTile<T>>) -> Option<T>
where
    T: enum_map::EnumArray<MenuTile<T>> + Copy,
{
    for (name, data) in tiles {
        if data.area.contains(mouse_position().into()) {
            return Some(name);
        }
    }
    None
}
