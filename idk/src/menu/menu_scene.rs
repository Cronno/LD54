use std::{collections::HashMap, marker::PhantomData, time::Duration};

use enum_map::Enum;
use macroquad::prelude::{is_mouse_button_down, MouseButton};
use serde::{Deserialize, Serialize};

use crate::{
    cursor_control::*,
    input::{InputCollector, InputHandler, InputType, MouseInput},
    scene::{Scene, SceneAction},
};

#[derive(Debug, Serialize, Deserialize, Enum, Clone, Copy, PartialEq, Eq, Hash)]
pub enum MenuInput {
    Up,
    Down,
    Left,
    Right,
    Confirm,
    Cancel,
}

pub trait Menu<C> {
    fn update(&mut self, _ctx: &C) {}
    fn confirm(&mut self, ctx: &C, mode: ControlMode, delta: Duration) -> SceneAction<C>;
    fn cancel(&mut self, ctx: &C, delta: Duration) -> SceneAction<C>;
    fn vertical_movement(&mut self, ctx: &C, dir: VDirection, inputs: &InputHandler<MenuInput>, delta: Duration);
    fn horizonal_movement(&mut self, ctx: &C, dir: HDirection, inputs: &InputHandler<MenuInput>, delta: Duration);
    fn render(&self, ctx: &C, mode: ControlMode);
}

#[derive(Debug, Clone, Copy)]
pub enum ControlMode {
    Button,
    Mouse,
}

pub struct MenuScene<C, T: Menu<C>> {
    pub menu: T,
    mode: ControlMode,
    v_control: CursorControl<VDirection>,
    h_control: CursorControl<HDirection>,

    inputs: InputHandler<MenuInput>,
    _phantom_data: PhantomData<C>,
}

impl<C, T: Menu<C>> MenuScene<C, T> {
    pub fn new(menu: T, input_config: &HashMap<MenuInput, Vec<InputType>>) -> Box<Self> {
        let inputs = InputHandler::new(input_config);
        Box::new(Self {
            menu,
            mode: ControlMode::Button,
            v_control: CursorControl::default(),
            h_control: CursorControl::default(),

            inputs,
            _phantom_data: PhantomData {},
        })
    }
}

impl<C, T: Menu<C>> Scene<C> for MenuScene<C, T> {
    fn handle_input(&mut self, input_events: &InputCollector) {
        self.inputs.update(input_events);
    }

    fn update(&mut self, ctx: &mut C, delta: Duration) -> SceneAction<C> {
        self.menu.update(ctx);
        self.update_mode();
        match self.mode {
            ControlMode::Button => self.button_mode_update(ctx, delta),
            ControlMode::Mouse => self.mouse_mode_update(ctx, delta),
        }
    }

    fn render(&self, ctx: &C) {
        self.menu.render(ctx, self.mode);
    }

    fn transition_out(&mut self, _ctx: &C) -> bool {
        self.inputs.clear();
        true
    }
}

impl<C, T: Menu<C>> MenuScene<C, T> {
    fn update_mode(&mut self) {
        if self.inputs.pressed(MenuInput::Up)
            || self.inputs.pressed(MenuInput::Down)
            || self.inputs.pressed(MenuInput::Left)
            || self.inputs.pressed(MenuInput::Right)
            || self.inputs.pressed(MenuInput::Confirm)
            || self.inputs.pressed(MenuInput::Cancel)
        {
            self.mode = ControlMode::Button;
        } else if self.inputs.mouse_delta().length() > 3.0 || is_mouse_button_down(MouseButton::Left) {
            self.mode = ControlMode::Mouse;
        }
    }

    fn button_mode_update(&mut self, ctx: &C, delta: Duration) -> SceneAction<C> {
        let (v_dir, h_dir) = get_movements(
            self.inputs.down(MenuInput::Up),
            self.inputs.down(MenuInput::Down),
            self.inputs.down(MenuInput::Left),
            self.inputs.down(MenuInput::Right),
        );
        let v_dist = self.v_control.handle_autoshift(v_dir, delta);
        let h_dist = self.h_control.handle_autoshift(h_dir, delta);

        if let Some(v_dir) = v_dir {
            for _ in 0..v_dist {
                self.menu.vertical_movement(ctx, v_dir, &self.inputs, delta);
            }
        }

        if let Some(h_dir) = h_dir {
            for _ in 0..h_dist {
                self.menu.horizonal_movement(ctx, h_dir, &self.inputs, delta);
            }
        }

        if self.inputs.pressed(MenuInput::Confirm) {
            self.menu.confirm(ctx, self.mode, delta)
        } else if self.inputs.pressed(MenuInput::Cancel) {
            self.menu.cancel(ctx, delta)
        } else {
            SceneAction::Continue
        }
    }

    fn mouse_mode_update(&mut self, ctx: &C, delta: Duration) -> SceneAction<C> {
        if self.inputs.mouse_pressed(MouseInput::Left) {
            self.menu.confirm(ctx, self.mode, delta)
        } else {
            SceneAction::Continue
        }
    }
}
