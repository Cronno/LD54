use std::time::Duration;

use keyframe::{
    functions::{EaseIn, EaseOut},
    keyframes, AnimationSequence,
};
use macroquad::{
    prelude::{Color, BLACK},
    shapes::draw_rectangle,
    window::{screen_height, screen_width},
};

use crate::{scene::Scene, timer::Timer};

enum TransitionState {
    Previous,
    Middle,
    Next,
}

pub(crate) enum AfterTransition {
    PushBoth,
    PushNext,
}

pub(crate) struct Transition<C> {
    animation: Box<dyn TransitionAnim<C>>,
    state: TransitionState,
    action: AfterTransition,
    previous_scene: Box<dyn Scene<C>>,
    next_scene: Box<dyn Scene<C>>,
    finished: bool,
}

impl<C> Transition<C> {
    pub fn new(
        animation: Box<dyn TransitionAnim<C>>,
        previous_scene: Box<dyn Scene<C>>,
        next_scene: Box<dyn Scene<C>>,
        action: AfterTransition,
    ) -> Self {
        Self {
            animation,
            state: TransitionState::Previous,
            action,
            previous_scene,
            next_scene,
            finished: false,
        }
    }

    pub fn update(&mut self, ctx: &C, delta: Duration) {
        match self.state {
            TransitionState::Previous => {
                if self.previous_scene.transition_out(ctx) {
                    self.state = TransitionState::Middle;
                }
            }
            TransitionState::Middle => {
                self.animation.update(delta);
                if self.animation.finished() {
                    self.state = TransitionState::Next;
                }
            }
            TransitionState::Next => {
                if self.next_scene.transition_in(ctx) {
                    self.finished = true;
                }
            }
        }
    }

    pub fn draw(&self, ctx: &C) {
        match self.state {
            TransitionState::Previous => self.previous_scene.render(ctx),
            TransitionState::Middle => {
                if !self.animation.is_half_finished() {
                    self.previous_scene.render(ctx);
                    self.animation.draw(ctx)
                } else {
                    self.next_scene.render(ctx);
                    self.animation.draw(ctx)
                }
            }
            TransitionState::Next => self.next_scene.render(ctx),
        }
    }

    pub fn finished(&self) -> bool {
        self.finished
    }

    pub fn end(self) -> (Box<dyn Scene<C>>, Box<dyn Scene<C>>, AfterTransition) {
        (self.previous_scene, self.next_scene, self.action)
    }
}

pub trait TransitionAnim<C> {
    fn update(&mut self, delta: Duration);
    fn draw(&self, ctx: &C);
    fn finished(&self) -> bool;
    fn is_half_finished(&self) -> bool;
}

pub struct Instant {}
impl Instant {
    pub fn base() -> Box<Self> {
        Box::new(Self {})
    }
}
impl<C> TransitionAnim<C> for Instant {
    fn update(&mut self, _delta: Duration) {}

    fn draw(&self, _ctx: &C) {}

    fn finished(&self) -> bool {
        true
    }

    fn is_half_finished(&self) -> bool {
        true
    }
}

pub struct Fade {
    sequence: AnimationSequence<f32>,
    color: Color,
}

impl Fade {
    pub fn new(color: Color) -> Box<Self> {
        let mut base = Self::base();
        base.color = color;
        base
    }

    pub fn base() -> Box<Self> {
        Box::new(Self {
            sequence: keyframes![(0.0, 0.0, EaseIn), (1.0, 0.3), (1.0, 0.8, EaseOut), (0.0, 1.1)],
            color: BLACK,
        })
    }
}

impl<C> TransitionAnim<C> for Fade {
    fn update(&mut self, delta: Duration) {
        self.sequence.advance_by(delta.as_secs_f64());
    }

    fn draw(&self, _ctx: &C) {
        let mut color = self.color;
        color.a = self.sequence.now();
        draw_rectangle(0.0, 0.0, screen_width(), screen_height(), color);
    }

    fn finished(&self) -> bool {
        self.sequence.finished()
    }

    fn is_half_finished(&self) -> bool {
        self.sequence.progress() >= 0.5
    }
}

pub struct WipeRight {
    width_timer: Timer,
    position_timer: Timer,
    color: Color,
}

impl WipeRight {
    pub fn new(color: Color) -> Box<Self> {
        Box::new(Self {
            width_timer: Timer::new(Duration::from_secs_f32(0.5)),
            position_timer: Timer::new(Duration::from_secs_f32(0.5)),
            color,
        })
    }

    pub fn base() -> Box<Self> {
        Box::new(Self {
            width_timer: Timer::new(Duration::from_secs_f32(0.5)),
            position_timer: Timer::new(Duration::from_secs_f32(0.5)),
            color: BLACK,
        })
    }
}

impl<C> TransitionAnim<C> for WipeRight {
    fn update(&mut self, delta: Duration) {
        if !<Self as TransitionAnim<C>>::is_half_finished(self) {
            self.width_timer.tick(delta);
        } else {
            self.position_timer.tick(delta);
        }
    }

    fn draw(&self, _ctx: &C) {
        if !<Self as TransitionAnim<C>>::is_half_finished(self) {
            let width = keyframe::ease(EaseOut, 0.0, screen_width() + 20.0, self.width_timer.percent());
            draw_rectangle(0.0, 0.0, width, screen_height(), self.color);
        } else {
            let x = keyframe::ease(EaseIn, 0.0, screen_width(), self.position_timer.percent());
            draw_rectangle(x, 0.0, screen_width(), screen_height(), self.color);
        }
    }

    fn finished(&self) -> bool {
        self.position_timer.finished()
    }

    fn is_half_finished(&self) -> bool {
        self.width_timer.finished()
    }
}
