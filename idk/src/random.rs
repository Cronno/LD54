// Implementation of the Alias Method for sampling a discrete probabilty distribution
// https://en.wikipedia.org/wiki/Alias_method
// https://www.keithschwarz.com/darts-dice-coins/

#[derive(Debug, Clone)]
pub struct LoadedDie {
    prob: Vec<f32>,
    alias: Vec<usize>,
}

impl LoadedDie {
    pub fn new(weights: &[f32]) -> Self {
        let n = weights.len();
        let mut scaled_prob: Vec<f32> = weights.iter().map(|p| p * n as f32).collect();

        let mut prob = vec![0.0; n];
        let mut alias = vec![0; n];
        let mut small = Vec::new();
        let mut large = Vec::new();

        for (i, p) in scaled_prob.iter().enumerate() {
            if *p < 1.0 {
                small.push(i);
            } else {
                large.push(i);
            }
        }

        while !small.is_empty() && !large.is_empty() {
            let l = small.pop().unwrap();
            let g = large.pop().unwrap();
            prob[l] = scaled_prob[l];
            alias[l] = g;
            scaled_prob[g] = scaled_prob[g] + scaled_prob[l] - 1.0;
            if scaled_prob[g] < 1.0 {
                small.push(g);
            } else {
                large.push(g);
            }
        }
        while let Some(g) = large.pop() {
            prob[g] = 1.0;
        }
        while let Some(l) = small.pop() {
            prob[l] = 1.0;
        }

        Self { prob, alias }
    }

    pub fn roll(&self) -> usize {
        let roll = fastrand::usize(0..self.prob.len());
        if fastrand::f32() < self.prob[roll] {
            roll
        } else {
            self.alias[roll]
        }
    }
}
