use std::time::Duration;

use crate::timer::Timer;

pub trait Direction {}
impl Direction for VDirection {}
impl Direction for HDirection {}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum VDirection {
    Up,
    Down,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum HDirection {
    Left,
    Right,
}

#[derive(Clone)]
pub struct CursorControl<T: Direction> {
    das_timer: Timer,
    arr_timer: Timer,
    prev_dir: Option<T>,
    zero_arr: bool,
}

impl<T: Direction + std::cmp::PartialEq> CursorControl<T> {
    pub fn handle_autoshift(&mut self, dir: Option<T>, delta: Duration) -> usize {
        // reset timers on direction change
        // move once in the new direction
        if self.prev_dir != dir {
            self.arr_timer.reset();
            self.das_timer.reset();
            self.prev_dir = dir;
            1
        } else if dir.is_some() {
            if !self.das_timer.finished() {
                self.das_timer.tick(delta);
                if self.das_timer.finished() {
                    self.arr_timer.tick(self.das_timer.overflow());
                }
                0
            } else if self.zero_arr {
                usize::MAX
            } else {
                self.arr_timer.tick(delta);
                let mut loops = 0;
                while self.arr_timer.finished() {
                    let overflow = self.arr_timer.overflow();
                    self.arr_timer.reset();
                    self.arr_timer.tick(overflow);
                    loops += 1;
                }
                loops
            }
        } else {
            0
        }
    }
}

impl<T: Direction> Default for CursorControl<T> {
    fn default() -> Self {
        Self {
            das_timer: Timer::new(Duration::from_millis(100)),
            arr_timer: Timer::new(Duration::from_millis(50)),
            prev_dir: None,
            zero_arr: false,
        }
    }
}

pub fn get_movements(up: bool, down: bool, left: bool, right: bool) -> (Option<VDirection>, Option<HDirection>) {
    let v_dir = match (up, down) {
        (true, false) => Some(VDirection::Up),
        (false, true) => Some(VDirection::Down),
        _ => None,
    };

    let h_dir = match (left, right) {
        (true, false) => Some(HDirection::Left),
        (false, true) => Some(HDirection::Right),
        _ => None,
    };

    (v_dir, h_dir)
}
