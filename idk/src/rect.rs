use macroquad::{prelude::{Rect, Color}, shapes::{draw_rectangle, draw_rectangle_lines}};
use serde::{Deserialize, Deserializer, Serialize};
use serde_with::{DeserializeAs, SerializeAs};

#[derive(Serialize, Deserialize)]
#[serde(remote = "Rect")]
pub struct RectDef {
    pub x: f32,
    pub y: f32,
    pub w: f32,
    pub h: f32,
}

impl SerializeAs<Rect> for RectDef {
    fn serialize_as<S>(value: &Rect, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        RectDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, Rect> for RectDef {
    fn deserialize_as<D>(deserializer: D) -> Result<Rect, D::Error>
    where
        D: Deserializer<'de>,
    {
        RectDef::deserialize(deserializer)
    }
}

pub fn rect_from_center(x: f32, y: f32, width: f32, height: f32) -> Rect {
    Rect {
        x: x - width / 2.0,
        y: y - height / 2.0,
        w: width,
        h: height,
    }
}

pub fn draw_rect(rect: &Rect, color: Color) {
    draw_rectangle(rect.x, rect.y, rect.w, rect.h, color)
}

pub fn draw_rect_lines(rect: &Rect, thickness: f32, color: Color) {
    draw_rectangle_lines(rect.x, rect.y, rect.w, rect.h, thickness, color)
}
